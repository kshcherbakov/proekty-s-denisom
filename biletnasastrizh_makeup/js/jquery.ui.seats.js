(function( $ ) {

    $.widget("ui.places", {
        options: {
            cartNumber: 0,
            placeCount: 0,
            freePlaces: [],
            price: 0,
			type: '',
			tooltip: true
        },

        PLACE_HTML: '<p class="place">' +
            '<span>  <b place="{place}">{place}</b> место </span>' +
            '<img src="./css/images/clos.png" width="22" height="22">' +
            '</p>',

        reserved: [],

        _create: function() {
            this._init();
            this.setFreePlaces();
            this._updateHtml();
            this.clearReserved();

            var that = this;
            this.placeEls.click(function() {
                that._selectPlace($(this));
            });
        },

        _init: function() {
            this.headEl = $(this.element);
            this.paneEl = $(this.element).next();
            this.placeEls = this.paneEl.find('p.car-placer, p.car-place');

            var o = this.options;
            o.placeCount = o.placeCount || this.placeEls.length;
            o.cartNumber = o.cartNumber || parseInt(this.placeEls.attr("car"), 10);
            //o.price = o.price;// || parseInt(this.headEl.find('.price').text(), 10);
        },

        _updateHtml: function() {
            var o = this.options;
			
            this.paneEl.find('.det b.busy').html(o.placeCount - o.freePlaces.length);
            this._replacePlaceName(this.paneEl.find('.det b.busy').next(), o.placeCount - o.freePlaces.length);

            this.paneEl.find('.det b:not(.busy)').html(o.freePlaces.length);
            this._replacePlaceName(this.paneEl.find('.det b:not(.busy)').next(), o.freePlaces.length);
			
			
			if(o.freePlaces.length == 0) {
				this.headEl.find('.f_places').html('<span class="red">0</span> Все места заняты');
				this.headEl.find('.price').text('Нет мест.').addClass('red');
			}
            else {
				this.headEl.find('.f_places span').html(o.freePlaces.length);
				this.headEl.find('.price').text(this.options.price + ' р.');
			}

			
			
            var description = this.paneEl.find('.det h3');
            description.text(description.text().replace(/\s([0-9]{2})/, " " + o.placeCount));
            this._replacePlaceName(description, o.placeCount);
            description.text(description.text().replace(/№[0-9]{1,2}/, "№ " + this.options.cartNumber));
        },

        _selectPlace: function(place) {
            if (this.placeEls.filter(place).length == 0 || place.is('[state="2"]')) return;
			
			if(this.options.tooltip) {
				this.options.tooltip = false;
				var tooltipPos = this.paneEl.find('.forClear').position();
				$('.tooltip').css({left: tooltipPos.left - 530, top: tooltipPos.top + 70}).fadeIn(1000,function() {
					setTimeout(function() {
						$('.tooltip').fadeOut(1000);
					}, 10000);
				});
			}
			
            var that = this,
                placeNum = place.attr('place'),
                reserved = place.is('[state="1"]');

            place.attr('state', reserved ? 0 : 1);
			var b = place.attr('state');
            if (reserved) {
				this.paneEl.find('div.det b.busy').text(parseInt(this.paneEl.find('div.det b.busy').text(),10) - 1);
				this.paneEl.find('div.det b:not(.busy)').text(parseInt(this.paneEl.find('div.det b:not(.busy)').text(),10) + 1);
				this.headEl.find('.f_places span').text(parseInt(this.headEl.find('.f_places span').text(),10) + 1);
			    this.paneEl.find('.line .place:has(b[place="' + placeNum + '"])').remove();
                this.reserved.splice($.inArray(placeNum, this.reserved), 1);
            } else {
				this.paneEl.find('div.det b.busy').text(parseInt(this.paneEl.find('div.det b.busy').text(),10) + 1);
				this.paneEl.find('div.det b:not(.busy)').text(parseInt(this.paneEl.find('div.det b:not(.busy)').text(),10) - 1);
				this.headEl.find('.f_places span').text(parseInt(this.headEl.find('.f_places span').text(),10) - 1);
                $(this.PLACE_HTML.replace(/{place}/g, placeNum)).appendTo(this.paneEl.find('.line')).find('img').click(function () {
                    var placeNum = $(this).closest('.place').find('span b').text();
                    that._selectPlace(that.placeEls.filter('[place="' + placeNum + '"]'));
                });

				this.reserved.push(placeNum);
                this.sortPlaces(this.paneEl.find('div.line'), "p.place", "b");
            }
            this.paneEl.find('.sum b').text(this.reserved.length * this.options.price + ' р.');
        },

        _replacePlaceName: function(element, placeCount) {
            var placeName = '',
                ost = placeCount % 10;

            if (ost == 1) placeName = 'место';
            if (ost >= 2 && ost <= 4) placeName = 'места';
            if (ost == 0 || ost > 4) placeName = 'мест';

            element.text(element.text().replace(/место|места|мест/, placeName));
        },

        _setOption: function(option, value) {
            if (option == 'freePlaces' || option == 'price' || option == 'placeCount')
                this._updateHtml();
        },

        getReserved: function() {
            return this.reserved;
        },

        getCar: function() {
            return this.options.cartNumber;
        },

        getType: function() {
            return this.options.type;
        },

        getPrice: function() {
            return this.options.price;
        },

        clearReserved: function() {
            this.placeEls.filter('[state="1"]').attr('state', 0);
            this.paneEl.find('.line p').remove();
            this.paneEl.find('.sum b').text('0 р.');
            this.reserved = [];
			this.headEl.find('.f_places span').html(this.options.freePlaces.length);
        },

        setFreePlaces: function () {
            var that = this,
                free = this.options.freePlaces;

            this.placeEls.attr('state', "2");

            if (free.length == 0) return;
            $.each(free, function (idx, place) {
                that.placeEls.filter('[place="' + place + '"]').attr("state", "0");
            });
        },
        sortPlaces: function (parent, childSelector, keySelector) {
            var items = parent.children(childSelector).sort(function (a, b) {
                var vB = parseInt($(keySelector, a).text(), 10);
                var vA = parseInt($(keySelector, b).text(), 10);
                return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
            });
            parent.append(items);
        }
    });

})(jQuery)
