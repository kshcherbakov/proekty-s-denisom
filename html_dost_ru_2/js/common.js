$(document).ready(function(){

  $("#header-contacts").tabs();

  $("div.popularList").columnize({ columns: 2, lastNeverTallest: true });

  //
  // Datepicker @ homepage
  //
  // Localize datepicker
  $.datepicker.setDefaults( $.datepicker.regional["ru"] );
  $.datepicker.setDefaults({
    nextText: '',
    prevText: '',
    minDate: 0,
    numberOfMonths: 2,
    showButtonPanel: true,
    closeText: 'Закрыть'
  });
  $.datepicker.setDefaults({
    beforeShow: function(input, inst) {
      $(input).parent("div.input-field-date").addClass("input-field-date-active");
    },
    onClose: function(input, inst) {
      $("div.input-field-date").removeClass("input-field-date-active");
    }
  })

  $("body.homePage #dateto").datepicker();
  $("body.homePage #dateback").datepicker();
  $("div.searchShedule div.input-field-date input").datepicker();

  // show datepicker by clicking on icon at Home page
  $(".input-field-date span").click(function(){
    $(this).prev("input").datepicker("show");
  });

  if($("div#calendarBlock").length == 1) {
    // show static datepicker
    $("div#calendarBlock input").datepicker({
      numberOfMonths: 1,
      showButtonPanel: false,
      showOtherMonths: true,
      selectOtherMonths: true,
      showAnim: "",
      beforeShow: function(input, inst) { 
        inst.dpDiv.addClass("ui-datepicker-static");
      },
      onSelect: function(input, inst) {
        $("div#calendarBlock input").datepicker("show");
        document.location.href = "/someurl/?date="+$("#calendarBlock input").val();
      }
    });
    $("div#calendarBlock input").datepicker("show");
    var offsetBlock = parseInt($("div#calendarBlock").offset().top)+4;
    $("div.ui-datepicker-static").css("top", offsetBlock +"px");
  }


  //
  // Show datepicker for a train page
  //
  if($("div#calendarBlock3").length == 1) {
    // show static datepicker
    var numberOfMonths = 3;
    var calendarWholeHeight;
    $("div#calendarBlock3 input").datepicker({
      numberOfMonths: numberOfMonths,
      showButtonPanel: false,
      showOtherMonths: false,
      selectOtherMonths: false,
      showAnim: "",
      maxDate: "+45d",
      beforeShow: function(input, inst) { 
        inst.dpDiv.addClass("ui-datepicker-static");
        calendarWholeHeight = (numberOfMonths * 230 + parseInt($("div#calendarBlock3 a.calendarBlockLink").height()) - 2) + "px" ;
        $("div#calendarBlock3 a.calendarBlockLink").css("top",calendarWholeHeight);
      },
      onSelect: function(input, inst) {
        $("div#calendarBlock3 input").datepicker("show");
        document.location.href = "/someurl/?date="+$("#calendarBlock3 input").val();
      }
    });
    $("div#calendarBlock3 input").datepicker("show");
    var offsetBlock = parseInt($("div#calendarBlock3").offset().top)+4;
    $("div.ui-datepicker-static").css("top", offsetBlock +"px");
  }

  //
  // Show datepicker for a search train page when ther're no trains for selected date (02_notrain.html)
  //
  if($("div#calendarBlock4").length == 1) {
    // show static datepicker
    var numberOfMonths = 3;
    $("div#calendarBlock4 input").datepicker({
      numberOfMonths: numberOfMonths,
      showButtonPanel: false,
      showOtherMonths: false,
      selectOtherMonths: false,
      showAnim: "",
      maxDate: "+45d",
      beforeShow: function(input, inst) { 
        inst.dpDiv.addClass("ui-datepicker-static");
        inst.dpDiv.addClass("ui-datepicker-static-horizontal");
        $("div#calendarBlock4 a.calendarBlockLink").css("top",calendarWholeHeight);
      },
      onSelect: function(input, inst) {
        $("div#calendarBlock4 input").datepicker("show");
        document.location.href = "/someurl/?date="+$("#calendarBlock4 input").val();
      }
    });
    $("div#calendarBlock4 input").datepicker("show");
    var offsetBlock = parseInt($("div#calendarBlock4").offset().top)+4;
    $("div.ui-datepicker-static").css("top", offsetBlock +"px");
  }




  /*
    Datepicker at Search train page
  */

  $("div.sheduleSearch-form input.sheduleSearch-date-input").datepicker({
    numberOfMonths: 1,
    maxDate: "+45d",
    beforeShow: function(input, inst) {
      $(input).next("span.sheduleSearh-date-span").addClass("sheduleSearh-date-span-active");
    },
    onClose: function(input, inst) {
      $("span.sheduleSearh-date-span").removeClass("sheduleSearh-date-span-active");
    }
  });
  // show datepicker by clicking on icon at Search page
  $("div.sheduleSearch-form span.sheduleSearh-date-span").click(function(){
    $(this).prev("input").datepicker("show");
  });


  // Show float header if body has an approrpiate class
  if($("body.floatHeader").length) {
    floatHeeder();
    $(document).scroll(function(){
      floatHeeder();
    });
  }

});

//
// function for setting of float header
//
function floatHeeder() {
      if($(this).scrollTop() > 70) $("body").addClass("scrolled");
      else  $("body").removeClass("scrolled");
}