
var bounds = [];
$(function () {
    bounds = [];
    $("#route").tabs({
        activate: function () {
            init();
        },
        create: function () {
            init();
        }
    });

    function init() {
        bounds = [
            $(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls") + " table").attr("startSt"),
            $(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls") + " table").attr("finishSt")
        ];
        bindClickSwitcher();
        setStationX();
        setMovers();
        setActiveTD(stationCenters[bounds[1]][0], [$(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls") + " .route-stations-finish > span"), 'finish']);
        setActiveTD(stationCenters[bounds[0]][0], [$(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls") + " .route-stations-start > span"), 'start']);
        setTitle();

        var codeS = $("div#" + $("div#route li.ui-tabs-active").attr("aria-controls") + " td.select-f span").attr("code");
        var codeD = $("div#" + $("div#route li.ui-tabs-active").attr("aria-controls") + " td.select-l span").attr("code");
        setRoute(codeS, codeD);
    }

    var wasMovie = [];
    $(".route-stations-start > span").mousedown(function (e) {
        var x = e.pageX;
        var helper = $(this);
        var stC = $(this).parents(".route-stations-start");
        var fC = $(this).parents(".route-stations-finish");
        var stC_W = stC.width();
        var maxW = fC.width() - $(this).width();
        $(document).bind('mousemove', function(e) {
            wasMovie[0] = helper;
            wasMovie[1] = "start";
            var newWidth = stC_W - (x - e.pageX);
            if (newWidth >= maxW) newWidth = maxW;
            if (newWidth <= helper.width()) newWidth = helper.width();
            stC.width(newWidth);
            setActiveTD(newWidth, wasMovie);
        });
    });
    $(".route-stations-finish > span").mousedown(function (e) {
        var x = e.pageX;
        var helper = $(this);
        var stC = $(this).siblings(".route-stations-start");
        var fC = $(this).parents(".route-stations-finish");
        var C = $(this).parents(".route-stations-select");
        var fC_W = fC.width();
        var maxW = C.width() - $(this).width();
        $(document).bind('mousemove', function (e) {
            wasMovie[0] = helper;
            wasMovie[1] = "finish";
            var newWidth = fC_W - (x - e.pageX);
            if (newWidth >= maxW) newWidth = maxW;
            if (newWidth <= stC.width() + helper.width()) newWidth = stC.width() + helper.width();
            fC.width(newWidth);
            setActiveTD(newWidth, wasMovie);
        });
    });
    $(document).mouseup(function (e) {
        $(document).unbind('mousemove');
        if (wasMovie[0]) finishMoving(wasMovie, bounds);
        wasMovie[0] = 0;
        wasMovie[1] = "";
    });
});

// �������� ������� ��� ���������
function setActiveTD(x, wasMovie) {
    var classTD = (wasMovie[1] == "start" ? "select-f" : "select-l");
    var table = wasMovie[0].parents(".route-stations-select").next("table");
    var td = table.find("td").eq(getNearestStation(x));
    if (!td.hasClass(classTD)) {
        if (!td.hasClass("select-f") && !td.hasClass("select-l")) {
            $("td", table).removeClass(classTD);
            $("td", table).removeClass("select-b");
            td.addClass(classTD);
            $("td.select-f", table).nextUntil("td.select-l").addClass("select-b");
        } else {
            $("td", table).removeClass(classTD);
            $("td", table).removeClass("select-b");
            td.addClass(classTD);
        }
    }
    setTitle();
}
// ���������� �������� ������ ��������� �������, ���������� ��� ���������� ������ ����
function finishMoving(obj) {
    var curX = obj[0].offset().left - obj[0].parents(".route-stations-select").offset().left;
    var dist = 10000;
    var iter = getNearestStation(curX);
    // ������ �� ��������� ������ � ������ �� ���� �������
    if (obj[1] == "start") {
        if (iter >= bounds[1]) iter--;
        bounds[0] = iter;
        $(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls") + " table").attr("startSt", iter);
    } else if (obj[1] == "finish") {
        if (iter <= bounds[0]) iter++;
        bounds[1] = iter;
        $(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls") + " table").attr("finishSt", iter);
    }
    setActiveTD(stationCenters[iter][0], obj);
    dist = curX - stationCenters[iter][0] - obj[0].width() / 2;
    obj[0].parent("div").animate({
        width: '-=' + dist
    });

    var codeS = $("div#" + $("div#route li.ui-tabs-active").attr("aria-controls") + " td.select-f span").attr("code");
    var codeD = $("div#" + $("div#route li.ui-tabs-active").attr("aria-controls") + " td.select-l span").attr("code");
    setRoute(codeS, codeD);
}
// ��������� ������� �� ����������
function getNearestStation(x) {
    for (i = 0; i < stationCenters.length; i++) {
        if (x >= stationCenters[i][1] && x <= stationCenters[i][2]) {
            return i;
        }
    };
    return -1; //if error
}

// ������ ����������� ��������
function bindClickSwitcher() {
    $("span.switch").off("click");
    $("span.switch", "#route .ui-tabs-active").click(function () {
        var switcher = $(this);
        var prev = switcher.prev();
        var next = switcher.next();
        var actArea = $("div#" + $(this).parents("li").attr("aria-controls")).children(".route-stations");
        prev.fadeOut('fast');
        switcher.fadeOut('fast');
        next.fadeOut('fast', function () {
            prev.attr("target", (prev.attr("target") == "from") ? "to" : "from");
            next.attr("target", (next.attr("target") == "from") ? "to" : "from");
            prev.fadeIn('fast');
            next.fadeIn('fast');
            switcher.fadeIn('fast');
            switchDirection(actArea.find("table"));
            setStationX();
            setMovers();
            setTitle();
            
            var codeS = $("#source").val();
            var codeD = $("#destination").val();
            setRoute(codeD, codeS);
        });
        return false;
    });
};

// ������ �������� �� ��� ���������� �������
function setMovers() {
    var actArea = $("#route div#" + $("li.ui-tabs-active").attr("aria-controls")).children(".route-stations");
    actArea.find(".route-stations-start").width(stationCenters[bounds[0]][0]);
    actArea.find(".route-stations-finish").width(stationCenters[bounds[1]][0]);
}

// ������ � ������������ ������ �������
var stationCenters = [];
function setStationX() {
    var obj = $(".route-info#" + $("li.ui-tabs-active", "#route").attr("aria-controls"));
    var allStationsWidth = 0;
    $("td", obj).each(function (i) {
        var thisWidth = $(this).width();
        var thisCenter = allStationsWidth + thisWidth / 2;
        stationCenters[i] = [];
        stationCenters[i][0] = thisCenter;
        stationCenters[i][1] = allStationsWidth;
        stationCenters[i][2] = allStationsWidth + thisWidth;
        allStationsWidth += thisWidth;
    });
}
// ��������� ����������� ��������
function switchDirection(obj) {
    var stations = [];
    $("td", obj).each(function (i) {
        stations[i] = $(this);
    });
    var activeTD = [];
    for (i = stations.length - 1; i >= 0; i--) {
        var newTD = stations[i].appendTo($("tr", obj));
        if (newTD.hasClass("select-l")) {
            newTD.removeClass("select-l").addClass("select-f");
            bounds[0] = stations.length - i - 1;
        } else {
            if (newTD.hasClass("select-f")) {
                newTD.removeClass("select-f").addClass("select-l");
                bounds[1] = stations.length - i - 1;
            }
        }
    }
}

//������������� ��������� �������� � ����� ����������� � ��������
function setTitle() {
    var textS = $("div#" + $("div#route li.ui-tabs-active").attr("aria-controls") + " td.select-f span").text();
    var textF = $("div#" + $("div#route li.ui-tabs-active").attr("aria-controls") + " td.select-l span").text();
    $(".routeTitle-from", ".routeTitle").text(textS);
    $(".routeTitle-to", ".routeTitle").text(textF);
}