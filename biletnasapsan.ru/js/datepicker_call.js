$(document).ready(function(){

  $(".dateSelect").click(function(){
      var html = $(".calendarLightbox-wrapper").html();
      $.colorbox({
          inline: true,
          href: '#calendarLightbox',
          close: 'Закрыть',
          overlayClose: false,
          className: true,
          onOpen: function () {
              $('#calendarLightbox').html(html);
              $('#cboxClose').removeClass('hidden');
          }
      });
  });

});