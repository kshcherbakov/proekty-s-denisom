var inCallback = false;
var placeThread = 0;
var oldPos = 0;
var currentLeft = 0;

var comeBack = "Херня какая-то";

function setDate(date) {
    //if ($('#calendar').hasClass('disabled')) return;
    //if (placeThread > 0) return;
    $("#date").val(date);
    //SetActiveDate(date);
    RequestTrains();
}

function setRoute(source, dest) {
    //if ($('#calendar').hasClass('disabled')) return;
    //if (placeThread > 0) return;
    $("#source").val(source),
    $("#destination").val(dest),
    //SetActiveDate(date);
    RequestTrains();
}

function RequestTrains() {
    if (inCallback) return;
    if (comeBack == '') return;

    OnStartTrainTableRequest();

    $.ajax({
        url: "/RailRequest/TrainTable",
        type: "POST",
        data: {
            from: $("#source").val(),
            to: $("#destination").val(),
            date: $("#date").val(),
            comeBack: comeBack
        },
        cache: false,
        success: OnEndTrainTableRequest,
        error: OnTrainRequestCallbackError
    });
}


function OnStartTrainTableRequest() {
    inCallback = true;
    //TODO: блокируем календарик 
    $('#calendar').fadeTo("fast", 0.2);
    $('#calendar').addClass('disabled');

    $("#trainTable").html("");
    $("#trainTable").css("height", "425px");

    ShowProgress();
}

function OnEndTrainTableRequest(response) {
    inCallback = false;
    //TODO: разблокируем календарик
    if (!response || response.type == "error" && response.errors.length > 0) {
        SetErrors(response.errors);
    } else {
        $("#trainTable").css("height", "");
        $("#trainTable").html(response);
    }
    //2 этап подгрузки информации о местах и билетах
    $('#calendar').fadeTo("fast", 0.2);
    $(".train-row").each(function () {
        placeThread++;
        LoadPlaceInfo($(this).attr("index"));
    });
    if (placeThread == 0) {
        $('#calendar').fadeTo("slow", 1.0);
        $('#calendar').removeClass('disabled');
    }
}

function OnTrainRequestCallbackError() {
    inCallback = false;
    SetErrors(["невозможно выполнить запрос"]);
}

function SetErrors(errors) {
    var errorMsg = "";
    for (var i = 0; i < errors.length; i++) {
        if (i > 0)
            errorMsg += "<br />";
        errorMsg += errors[i];
    }
    var html = "<div style='margin: 0 auto; font-size: 24px; padding-top: 166px; color: #B91419; text-align: center;'>Ошибка: " + errorMsg + "</div>";
    $("#trainTable").html(html);
}

function ShowProgress() {
    var html = "<div style='margin: 0 auto; padding-top: 166px; text-align: center;'>Загрузка<br /><br /><img src='/Content/images/ajax-loader.gif' alt='Загрузка' /></div>";
    $("#trainTable").html(html);
}

function LoadPlaceInfo(index) {
    $("#PlaceInfo_" + index).html("<div style='margin: 0 auto; text-align: center;'>Загрузка<br /><br /><img src='/Content/images/ajax-loader.gif' alt='Загрузка' /></div>");
    var date = $("#date").val();
    var from = $("#Source_" + index).val();
    var to = $("#Destination_" + index).val();
    var number = $("#Number_" + index).val();
    var trainInfo = {
        "Number": $("#Number_" + index).val(),
        "Source": $("#Source_" + index).val(),
        "Destination": $("#Destination_" + index).val(),
        "TimeBegin": $("#TimeBegin_" + index).val(),
        "TimeEnd": $("#TimeEnd_" + index).val(),
        "TimeSpent": $("#TimeSpent_" + index).val(),
        "PriceC1": 0,
        "PriceC2": 0,
        "PlaceC1": 0,
        "PlaceC2": 0,
        "Firm": $("#Firm_" + index).val()
    };
    $.ajax({
        url: "/RailRequest/CategoryPlace",
        type: "POST",
        data: {
            from: from,
            to: to,
            date: date,
            number: number,
            index: index,
            trainInfo: $.toJSON(trainInfo),
            comeBack: comeBack
        },
        cache: false,
        success: function(response) {
            placeThread--;
            if (placeThread == 0) {
                $('#calendar').fadeTo("slow", 1.0);
                $('#calendar').removeClass('disabled');
            }
            if (!response || response.type == "error" && response.errors.length > 0) {
                $("#PlaceInfo_" + index).html("<div style='margin: 0 auto; text-align: center;'>" + response.errors + "</div>");
            } else {
                $("#PlaceInfo_" + index).html(response);
            }
            $('a.tooltip').cluetip({ width: 400, activation: 'click', showTitle: false, height: 190 });
        },
        error: function() {
            placeThread--;
            if (placeThread == 0) {
                $('#calendar').fadeTo("slow", 1.0);
                $('#calendar').removeClass('disabled');
            }
            $("#PlaceInfo_" + index).html("<div style='margin: 0 auto; text-align: center;'>Во время загрузки произошла ошибка...<br /><br /><a href='javascript:;' onclick='LoadPlaceInfo(" + index + ")'>Обновить повторно</a></div>");
        }
    });
}

$(function () {

    var liWidth = $(".monthsWrapper li:first").width();
    var monthShift = parseInt($(".monthList ul").css("left"));
    var monthLength = liWidth * $(".monthList li").length - $(".monthsWrapper").width();
    var animateLeft = 0;
    var animateRight = 0;
    var mousePressed = 0;
    var selector = $(".monthDateSelect");

    if (monthShift != monthShift) monthShift = 0;
    SetActiveDate($(".monthsWrapper li.monthDay-selected").attr("date"));
   
    // Установка начальной позиции календаря при загрузке страницы
    var activeLiIndex = $(".monthList ul li").index($(".monthList ul li.monthDay-selected"));
    var end = Math.floor(liWidth * activeLiIndex + monthShift); //alert(end + " : " + monthLength);
    if(monthLength - end + liWidth*5 > 0) {
      if(activeLiIndex > 5) {
         monthShift = parseInt($(".monthList").css("left")) - end + liWidth*5;
         $("#arrowLeft").removeClass("calendarArrows-left-inactive");
      }
    } else {
      monthShift = -monthLength;
      $("#arrowLeft").removeClass("calendarArrows-left-inactive");
      $("#arrowRight").addClass("calendarArrows-right-inactive");
    }
    $(".monthList").css("left", monthShift + "px");
    moveToLi(selector, $(".monthList ul li.monthDay-selected"));
    
    RequestTrains();
    
    $("li[month]", "div.monthList").each(function () {
        $(this).prepend("<span class='monthName'>" + $(this).attr("month") + "</span>");
    });

    selector.mousedown(function (e) {
        var selector = $(this);
        var selectorX = parseInt(selector.css("left"));
        var x = e.pageX;
        var maxX = $(".monthsWrapper").width() - selector.width();
        mousePressed = 1;
        $(document).bind('mousemove', function (e) {
            var newX = selectorX + (e.pageX - x);
            //$(".monthName").text(newX +" : "+ maxX);
            if (newX >= 0 && newX <= maxX) {
                selector.css("left", newX);
                setActiveLi(newX);
            }
            if (newX >= maxX) {
                //shiftULLeft();
                //$(".monthName").text(newX +" : "+ maxX + " : влево ");
            }
            if (newX <= 0) {
                //$(".monthName").text(newX +" : "+ maxX + " : вправо ");
                //shiftULRight();
            }
        });
    });
    $(document).mouseup(function (e) {
        $(document).unbind('mousemove');
        if (mousePressed) {
            moveToLi(selector, $(".monthList ul li.monthDay-selected"));
            mousePressed = 0;
        }
    });
    
    //$(".date-item").mousedown(function(){
      
    //});

    function setActiveLi(x) {
        $(".monthList li").removeClass("monthDay-selected");
        var activeLi = $(".monthList li").eq(getLi(x));
        activeLi.addClass("monthDay-selected");
        SetActiveDate(activeLi.attr("date"));
    }

    function moveToLi(selector, activeLi) {
        var activeLiIndex = $(".monthList ul li").index(activeLi);
        var end = Math.floor(liWidth * activeLiIndex + monthShift);
        if (!activeLi.hasClass("monthDay-selected")) $(".monthList li").removeClass("monthDay-selected");
        selector.animate({
            left: end
        },
      200,
      'linear',
      function () {
          if (!activeLi.hasClass("monthDay-selected")) {
              $(".monthList li").removeClass("monthDay-selected");
              activeLi.addClass("monthDay-selected");
              SetActiveDate(activeLi.attr("date"));
          }
          RequestTrains();
      });
        //$(".monthName").text(liWidth +" : "+ activeLiIndex +" : "+ monthShift);
    }

    //
    // Выдает ближайшую дату рядом с заданной координатой
    // 
    function getLi(x) {
        return Math.floor((x - monthShift + 15) / liWidth);
    }

    //
    // Прокрутка дней влево и вправо
    //
    $("#arrowRight").mousedown(function () {
        if (!$(this).hasClass("calendarArrows-right-inactive")) {
            animateLeft = 1;
            shiftULLeft($(".monthList"), 18, 0);
            $("#arrowLeft").removeClass("calendarArrows-left-inactive");
        }
    });
    $("#arrowRight").mouseup(function () {
        animateLeft = 0;
        monthShift = parseInt($(".monthList").css("left"));
        //moveToLi(selector, $(".monthDay-selected",".monthList"));
    });

    $("#arrowLeft").mousedown(function () {
        if (!$(this).hasClass("calendarArrows-left-inactive")) {
            animateRight = 1;
            shiftULRight($(".monthList"), 18, 0);
            $("#arrowRight").removeClass("calendarArrows-right-inactive");
        }
    });
    $("#arrowLeft").mouseup(function () {
        animateRight = 0;
        monthShift = parseInt($(".monthList").css("left"));
        //moveToLi(selector, $(".monthDay-selected",".monthList"));
    });

    var timeStep = 50;

    function shiftULLeft(obj, step, i) {
        if (animateLeft) {
            // текущая позиция
            var curPosition = parseInt(obj.css("left"));
            //$(".dateSelect-title").text(i +":" +curPosition +":"+ step +":"+ monthLength);
            // если не выходим за длину линейки дней
            if (Math.abs(curPosition + step) < monthLength) {
                // ускоряем движение на n шаге
                if (i > 20) step = 18;
                if (i > 30) step = 36;
                if (i > 40) step = 54;
                i++;
                // двигаем линейку дней
                obj.css("left", curPosition - step + "px");
                // двигаем ползунок выбора дней
                selector.css("left", parseInt(selector.css("left")) - step + "px");
                // следующая итерация
                setTimeout(function () { shiftULLeft(obj, step, i) }, timeStep);
                return;
            } else {
                // если достигнуто крайнее значение, ставим в крайнюю точку и обнуляем флаг активной анимации
                obj.css("left", -monthLength + "px");
                animateLeft = 0;
                // disable для кнопки
                $("#arrowRight").addClass("calendarArrows-right-inactive");
                return;
            }
        } else {
            return;
        }
    }

    function shiftULRight(obj, step, i) {
        if (animateRight) {
            var curPosition = parseInt(obj.css("left"));
            //$(".dateSelect-title").text(curPosition +":"+ step +":"+ (curPosition+step));
            if (curPosition + step <= 0) {
                // ускоряем движение на n шаге
                if (i > 20) step = 20;
                if (i > 30) step = 30;
                if (i > 40) step = 40;
                i++;
                obj.css("left", curPosition + step + "px");
                selector.css("left", parseInt(selector.css("left")) + step + "px");
                setTimeout(function () { shiftULRight(obj, step, i) }, timeStep);
                return;
            } else {
                obj.css("left", 0 + "px");
                animateLeft = 0;
                $("#arrowLeft").addClass("calendarArrows-left-inactive");
                return;
            }
        } else {
            return;
        }
    }

    $("li.date-item").click(function () {
        moveToLi(selector, $(this));
    });
    
    
    function SetActiveDate(date) {
 
      var after = new Array();
      var before = new Array();
      var pos = 0; var go = false;

      $(".date-item").each(function() {
        if (!go && $(this).hasClass('monthDay-selected')) {
            go = true;
        }
        if (!go) {
            pos++;
            before.push($(this));
        } else {
            after.push($(this));
        }
      });

      if (oldPos != pos ) {
        //if (pos > 8 + currentLeft) slideRight(1);
        //if (pos < 8 + currentLeft) slideLeft(1);
        oldPos = pos;
      }

      var fontsize = 100;
      if (after.length > 0) {
        for (var i = 0; i < after.length; i++) {
            if (fontsize > 40) fontsize -= 4;
            after[i].find('span.monthDay').css('font-size', fontsize + '%');
        }
      }

      fontsize = 100;
      if (before.length > 0) {
        for (var i = before.length - 1; i >= 0; i--) {
            if (fontsize > 40) fontsize -= 4;
            before[i].find('span.monthDay').css('font-size', fontsize + '%');
        }
      }
}

    
});