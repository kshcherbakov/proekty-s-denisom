﻿var currentLeft = 0;
var itemW = 0;
var totalCount = 0;
var inCallback = false;
var comeBack = '';
var oldPos = 0;
var placeThread = 0;
var isReturned = false;


$(document).ready(function() {
    itemW = $('.monthList').find('li:first').width();
    totalCount = $(".monthList li").size();
    $('#calendar').removeClass('disabled');
    $('#calendar').show();
    setDate($("#date").val());

    var currentPos = $('.monthDay-selected').attr('num');
    if (currentPos > 15) slideRight(currentPos - 15);
    
    $("#arrowRight").on("click", function (event) {
      if ($('#calendar').hasClass('disabled')) return;
      slideRight(15);
    });

    $("#arrowLeft").on("click", function (event) {
      if ($('#calendar').hasClass('disabled')) return;
      slideLeft(15);
    });
    
});

function RequestTrains() {
    if (inCallback) return;
    if (comeBack == '') return;

    OnStartTrainTableRequest();
    $.ajax({
        url: "/RailRequest/TrainTable",
        type: "POST",
        data: {
            from: $("#source").val(),
            to: $("#destination").val(),
            date: $("#date").val(),
            comeBack: comeBack
        },
        cache: false,
        success: OnEndTrainTableRequest,
        error: OnTrainRequestCallbackError
    });
}


function OnStartTrainTableRequest() {
    inCallback = true;
    //TODO: блокируем календарик 
    $('#calendar').fadeTo("fast", 0.2);
    $('#calendar').addClass('disabled');

    $("#trainTable").html("");
    $("#trainTable").css("height", "425px");

    ShowProgress();
}

function OnEndTrainTableRequest(response) {
    inCallback = false;
    //TODO: разблокируем календарик
    if (!response || response.type == "error" && response.errors.length > 0) {
        SetErrors(response.errors);
    } else {
        $("#trainTable").css("height", "");
        $("#trainTable").html(response);
    }
    //2 этап подгрузки информации о местах и билетах
    $('#calendar').fadeTo("fast", 0.2);
    $(".train-row").each(function() {
        placeThread++;
        LoadPlaceInfo($(this).attr("index"));
    });
    if (placeThread == 0) {
        $('#calendar').fadeTo("slow", 1.0);
        $('#calendar').removeClass('disabled');
    }
}

function OnTrainRequestCallbackError() {
    inCallback = false;
    SetErrors(["невозможно выполнить запрос"]);
}

function OnChangeInputCount(train, place, val) {    
    $("#PlaceC" + place + train).val(val);
}

function OnSetPlaceC(train, place, val) {
    
    if ( comeBack == "True" ) {
        alert('В обратном билете нельзя менять количество пассажиров.');
        return false;
    }
    var maxValue = $("#PlaceC" + place + '_' + train + '_Count').val();
    var obj = $("#count" + train + "_" + place);
    var i = val;
    if (obj.val() != "") {
        i = parseInt(obj.val()) + val;
    }
    if (i >= 0 && i <= maxValue ) {
        obj.val(i);
        $("#PlaceC" + place + train).val(i);
    }
}

function SetErrors(errors) {
    var errorMsg = "";
    for (var i = 0; i < errors.length; i++) {
        if (i > 0)
            errorMsg += "<br />";
        errorMsg += errors[i];
    }
    var html = "<div style='margin: 0 auto; font-size: 24px; padding-top: 166px; color: #B91419; text-align: center;'>Ошибка: " + errorMsg + "</div>";
    $("#trainTable").html(html);
}

function ShowProgress() {
    var html = "<div style='margin: 0 auto; padding-top: 166px; text-align: center;'>Загрузка<br /><br /><img src='/img/ajax-loader.gif' alt='Загрузка' /></div>";
    $("#trainTable").html(html);
}


function slideRight(count) {
    if ($("#arrowRight").hasClass('calendarArrows-right-inactive')) return;
    $("#arrowLeft").removeClass('calendarArrows-left-inactive');
    currentLeft += count;
    if (currentLeft > totalCount - count) currentLeft = totalCount - count;
    $(".monthList").animate({ "left": "-" + (currentLeft * itemW) - 10 + "px" }, "slow");
    if (currentLeft == totalCount - count) $("#arrowRight").addClass('calendarArrows-right-inactive');
}

function slideLeft(count) {
    if ($("#arrowLeft").hasClass('calendarArrows-left-inactive')) return;
    $("#arrowRight").removeClass('calendarArrows-right-inactive');
    currentLeft -= count;
    if (currentLeft < 0) currentLeft = 0;
    var left = (currentLeft * itemW) + 10;
    if (left <= 10) left = 0;
    $(".monthList").animate({ "left": "-" + left + "px" }, "slow");
    if (currentLeft == 0) $("#arrowLeft").addClass('calendarArrows-left-inactive');
}

function setDate(date) {
    if ($('#calendar').hasClass('disabled')) return;
    if (placeThread > 0) return;
    $("#date").val(date);
    SetActiveDate(date);
    RequestTrains();
}


function SetActiveDate ( date ) {
 
    $(".date-item").removeClass('monthDay-selected');
    $('.date-item[date="' + date + '"]').addClass('monthDay-selected');    

    var after = new Array();
    var before = new Array();
    var pos = 0; var go = false;

    $(".date-item").each(function() {
        if (!go && $(this).hasClass('monthDay-selected')) {
            go = true;
        }
        if (!go) {
            pos++;
            before.push($(this));
        } else {
            after.push($(this));
        }
    });

    if (oldPos != pos ) {
        if (pos > 8 + currentLeft) slideRight(1);
        if (pos < 8 + currentLeft) slideLeft(1);
        oldPos = pos;
    }

    //if ( currentPos > 15 ) slideRight(currentPos);

    var fontsize = 100;
    if (after.length > 0) {
        for (var i = 0; i < after.length; i++) {
            if (fontsize > 40) fontsize -= 4;
            after[i].find('span:first').css('font-size', fontsize + '%');
        }
    }

    fontsize = 100;
    if (before.length > 0) {
        for (var i = before.length - 1; i >= 0; i--) {
            if (fontsize > 40) fontsize -= 4;
            before[i].find('span:first').css('font-size', fontsize + '%');
        }
    }
}


function LoadPlaceInfo(index) {   
    $("#PlaceInfo_" + index).html("<div style='margin: 0 auto; text-align: center;'>Загрузка<br /><br /><img src='/img/ajax-loader.gif' alt='Загрузка' /></div>");
    var date = $("#date").val();
    var from = $("#Source_" + index).val();
    var to = $("#Destination_" + index).val();
    var number = $("#Number_" + index).val();
    var trainInfo = {
        "Number": $("#Number_" + index).val(),
        "Source": $("#Source_" + index).val(),
        "Destination": $("#Destination_" + index).val(),
        "TimeBegin": $("#TimeBegin_" + index).val(),
        "TimeEnd": $("#TimeEnd_" + index).val(),
        "TimeSpent": $("#TimeSpent_" + index).val(),
        "PriceC1": 0,
        "PriceC2": 0,
        "PlaceC1": 0,
        "PlaceC2": 0,
        "Firm": $("#Firm_" + index).val()
    };
    $.ajax({
        url: "/RailRequest/CategoryPlace",
        type: "POST",
        data: {
            from: from,
            to: to,
            date: date,
            number: number,
            index: index,
            trainInfo: $.toJSON(trainInfo),
            comeBack: comeBack
        },
        cache: false,
        success: function (response) {
            placeThread--;
            if (placeThread == 0) {
                $('#calendar').fadeTo("slow", 1.0);
                $('#calendar').removeClass('disabled');
            }
            if (!response || response.type == "error" && response.errors.length > 0) {
                $("#PlaceInfo_" + index).html("<div style='margin: 0 auto; text-align: center;'>" + response.errors + "</div>");
            } else {
                $("#PlaceInfo_" + index).html(response);
            }
            $('a.tooltip').cluetip({ width: 400, activation: 'click', showTitle: false, height: 190 });
        },
        error: function () {
            placeThread--;
            if (placeThread == 0) {
                $('#calendar').fadeTo("slow", 1.0);
                $('#calendar').removeClass('disabled');
            }
            $("#PlaceInfo_" + index).html("<div style='margin: 0 auto; text-align: center;'>Во время загрузки произошла ошибка...<br /><br /><a href='javascript:;' onclick='LoadPlaceInfo(" + index + ")'>Обновить повторно</a></div>");
        }
    });
}