﻿$(function () {
    $(".towns a").toggle(function() {
        $(this).parents(".towns").find("ul").slideDown("fast");
        return false;
    },
        function() {
            $(this).parents(".towns").find("ul").slideUp("fast");
            return false;
        });
});
//hs.marginTop = 270;
//hs.anchor = 'top left';


function PostCity(from, to) {
    $('#from').val(from);
    $('#to').val(to);
    if ($('#main-form').attr('id') == undefined) location.href = '/';
    $('#main-form').submit();
}

function PostCityBack(from, to) {
    $('#from').val(from);
    $('#to').val(to);
    $('#back').val('True');
    if ($('#main-form').attr('id') == undefined) location.href = '/';
    $('#main-form').submit();
}


$(function () {
    $("#getCall").mask("8(999)9999999");
    $("#getCall").defaultValue();

    $("div.profile-link a").click(function () {
        $("div.profile").toggleClass("profile-opened");
        return false;
    });

    $("#submitRequest").submit(function () {
        $("#submitButton").attr("disabled", "disabled");
        var message = "";
        $.ajax({
            type: 'POST',
            cache: false,
            dataType: 'JSON',
            url: $(this).attr("action"),
            data: $(this).serialize(),
            success: function (response) {
                message = "Ваш запрос на вызов оператора принят.";
                if (response.message == "ok") {
                    $("#callback-opening").html("<b>Ждите звонок оператора</b>");
                    $("div.profile").removeClass("profile-opened");
                } else if (response.message == "empty")
                    message = "Вы не ввели номера телефона";

                $("#getCall").val("");
                $("#submitButton").removeAttr("disabled");

                alert(message);
            }
        });
        return false;
    });

    $("#index").click(function () {
        $('#cluetip').hide();
    });
});