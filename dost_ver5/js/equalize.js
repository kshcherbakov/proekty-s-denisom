// Artfinder Equal Height Children plugin
// https://github.com/artfinder/jquery-equal-height
// version 1.0


(function($) {
    
    $.fn.make_children_equal_height = function (attr) {
        var make_equal_height = function (element, attr) {
            var $within = $(element),
                $attr = (attr ? attr : 'data-equal-height');
                selector = $within.attr($attr),
                $children = $(selector, $within),
                tallest = 0;
            
            $children.each( function() {
                var $this = $(this);
                
                $this.css('height', '');
                var h = $this.outerHeight();
                if ( h > tallest ) {
                    tallest = h;
                }
            });
            
            $children.each( function() {
                $(this).css('min-height', tallest + 'px');
            });
        };
        
        return this.each( function () {
            make_equal_height(this, attr);
        });
    };

$.fn.make_children_disequal_height = function (attr) {
  var make_disequal_height = function (element, attr) {
    var $within = $(element),
        $attr = (attr ? attr : 'data-equal-height');
    selector = $within.attr($attr),
    $children = $(selector, $within);
    $children.each( function() {
	    $(this).css('min-height', '');
    });
  };
  return this.each( function () {
    make_disequal_height(this);
  });
};
    
})( jQuery );