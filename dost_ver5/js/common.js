(function($){
  
  $(".otherForm-tabs a").hover(function(){
    var icon = $(this).find(".icon");
    var icon_class = icon.attr("data-icon");
    icon.removeClass("icon-" + icon_class + "-blue");
    icon.addClass("icon-" + icon_class + "-white");
  },function(){
    var icon = $(this).find(".icon");
    var icon_class = icon.attr("data-icon");
    icon.addClass("icon-" + icon_class + "-blue");
    icon.removeClass("icon-" + icon_class + "-white");
  });
  
  if($('.equal-height').length) {
    $('[data-equal-height]').make_children_equal_height('data-equal-height');
    $(window).resize(function() { 
      $('[data-equal-height]').make_children_equal_height('data-equal-height')
    });
  }
  
  $(".header-black-nav-headerGrayLink").click(function(){
    $(".header").toggleClass("header-gray-opened");
  })
  $(".header-black-nav-link").click(function(){
    $(".header").toggleClass("header-black-nav-opened");
  })
 
 
  //
  // ������ ������ ����, �������������
  //
  $(".findForm-date-oneWay input").click(function(){
    if($(this).prop("checked")) {
      $("#route_when_back").attr("disabled","true");
    } else {
      $("#route_when_back").removeAttr("disabled");
    }
  })

  //
  // ������������, ��� 2, ����� ����
  //
  $(".bookingWagons-item-choose").click(function(){
    $(this).parents(".bookingTable-item").toggleClass("opened");
    return false;
  });
  $(".bookingWagons-item-chooseClose").click(function(){
    $(this).parents(".bookingTable-item").toggleClass("opened");
    return false;
  });
  
  // ���� �����/������
  $("[data-tab='tab']").each(function(){
    var tab = $(this);
    tab.find("[data-tab='selector']").each(function(){
      var selector = $(this);
      selector.click(function(){
        var target = selector.attr("data-controls");
        tab.find("[data-tab='panel']").removeClass("active");
        tab.find(target+"[data-tab='panel']").addClass("active");
        
        tab.find("[data-tab='selector']").removeClass("active");
        selector.addClass("active");
        return false;
      })
    })
  })
  
  //
  // ������������, ��� 3, ���� ������
  //
  $(".ticketPersonal-item-addChildren").click(function(){
    $(this).parents("[data-role='passangerContainer']").addClass("ticketPersonal-item-childOpen");
    scrollToElem($($(this).attr("data-href")));
    return false;
  });
  $(".ticketPersonal-item-closeChildren").click(function(){
    $(this).parents("[data-role='passangerContainer']").removeClass("ticketPersonal-item-childOpen");
    scrollToElem($($(this).attr("data-href")));
    return false;
  });
 
  function scrollToElem(elem) {
    if(window.width()<768) {
      $("html, body").animate({
        scrollTop: elem.offset().top + "px"
      },  {
        duration: 1000
      });
      return false;
    }
  }
  
  //
  // ������ �������
  //
  if($(".orders").length) {
    $(".orders-item-showDetails").click(function(){
      $(this).parent().toggleClass("opened");
      return false;
    });
    $(".orders-item-close").click(function(){
      $(this).parent().toggleClass("opened");
      return false;
    });
  }


  //
  // ������� � ������ ���������
  //
  if($(".routePopular").length) {
    if($(".routePopular-list-letterSection").length > 1) {
      $(".routePopular-list").columnize({
        columns: 2
      });
    } else {
      $(".routePopular-list-letterSection").columnize({
        columns: 2
      });
    }
  }
  
  //
  // ������� � ������ �������
  //
  if($(".stations").length) {
      $(".stations-list").columnize({
        columns: 3,
        lastNeverTallest: true
      });
  }
  
  
  //
  // ��������� �������
  //
  $("[data-swiper=true]").each(function(){
    $(this).append("<div class='swiper-scrollbar'></div>");
    var scrollBar = $(this).find(".swiper-scrollbar");
    var swiper = new Swiper ($(this), {
      freeMode: true,
      slideClass: 'swiper-item',
      wrapperClass: 'swiper-wrapper',
      slidesPerView: 'auto',
      scrollbar: '.swiper-scrollbar',
      scrollbarHide: false
    });
  });
  
  
  $(".leaveReview-rating-item").mouseover(function(){
    $(this).addClass("selected");
    $(this).prevAll().addClass("selected");
    $(this).nextAll().removeClass("selected");
  });
  $(".leaveReview-rating-item").click(function(){
    $(this).addClass("selected");
    $(this).prevAll().addClass("selected");
    $(this).nextAll().removeClass("selected");
    return false;
  });

  
})(jQuery)