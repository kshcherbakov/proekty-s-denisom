


<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>build/crosspixel.js at master from aishek/crosspixel - GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />

    
    

    <meta content="authenticity_token" name="csrf-param" />
<meta content="Prwfst9HVoNSqZld99H+aIHd45w3zG0TUE/8ELbl9ho=" name="csrf-token" />

    <link href="https://a248.e.akamai.net/assets.github.com/stylesheets/bundles/github-92243db0daab07aa944d353c1ba062b9581bd321.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="https://a248.e.akamai.net/assets.github.com/stylesheets/bundles/github2-87ee95a5f2db56a1f4a7182cbe29182e4b9eeccd.css" media="screen" rel="stylesheet" type="text/css" />
    

    <script src="https://a248.e.akamai.net/assets.github.com/javascripts/bundles/jquery-1e75833f765514d7e7efe0099020c2ad4ff15e5b.js" type="text/javascript"></script>
    <script src="https://a248.e.akamai.net/assets.github.com/javascripts/bundles/github-9651d1f774dd0986ce216e63d28311841387fa40.js" type="text/javascript"></script>
    

      <link rel='permalink' href='/aishek/crosspixel/blob/829fd6f9f56c31ca76e8e06730aa8e1f1f277858/build/crosspixel.js'>

    <meta name="description" content="crosspixel - Javascript-утилита, которая позволяет накладывать макет поверх вёрстки кроссбраузерно" />
  <link href="https://github.com/aishek/crosspixel/commits/master.atom" rel="alternate" title="Recent Commits to crosspixel:master" type="application/atom+xml" />

  </head>


  <body class="logged_out page-blob windows vis-public env-production ">
    


    

      <div id="header" class="true clearfix">
        <div class="container clearfix">
          <a class="site-logo" href="https://github.com">
            <!--[if IE]>
            <img alt="GitHub" class="github-logo" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7.png?1323882716" />
            <img alt="GitHub" class="github-logo-hover" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7-hover.png?1324325358" />
            <![endif]-->
            <img alt="GitHub" class="github-logo-4x" height="30" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7@4x.png?1323882716" />
            <img alt="GitHub" class="github-logo-4x-hover" height="30" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7@4x-hover.png?1324325358" />
          </a>

                  <!--
      make sure to use fully qualified URLs here since this nav
      is used on error pages on other domains
    -->
    <ul class="top-nav logged_out">
        <li class="pricing"><a href="https://github.com/plans">Signup and Pricing</a></li>
        <li class="explore"><a href="https://github.com/explore">Explore GitHub</a></li>
      <li class="features"><a href="https://github.com/features">Features</a></li>
        <li class="blog"><a href="https://github.com/blog">Blog</a></li>
      <li class="login"><a href="https://github.com/login?return_to=%2Faishek%2Fcrosspixel%2Fblob%2Fmaster%2Fbuild%2Fcrosspixel.js">Login</a></li>
    </ul>



          
        </div>
      </div>

      

            <div class="site">
      <div class="container">
        <div class="pagehead repohead instapaper_ignore readability-menu">


        <div class="title-actions-bar">
          <h1>
            <a href="/aishek">aishek</a> /
            <strong><a href="/aishek/crosspixel" class="js-current-repository">crosspixel</a></strong>
          </h1>
          



              <ul class="pagehead-actions">


          <li><a href="/login?return_to=%2Faishek%2Fcrosspixel" class="minibutton btn-watch watch-button entice tooltipped leftwards" rel="nofollow" title="You must be logged in to use this feature"><span><span class="icon"></span>Watch</span></a></li>
          <li><a href="/login?return_to=%2Faishek%2Fcrosspixel" class="minibutton btn-fork fork-button entice tooltipped leftwards" rel="nofollow" title="You must be logged in to use this feature"><span><span class="icon"></span>Fork</span></a></li>


      <li class="repostats">
        <ul class="repo-stats">
          <li class="watchers ">
            <a href="/aishek/crosspixel/watchers" title="Watchers" class="tooltipped downwards">
              16
            </a>
          </li>
          <li class="forks">
            <a href="/aishek/crosspixel/network" title="Forks" class="tooltipped downwards">
              3
            </a>
          </li>
        </ul>
      </li>
    </ul>

        </div>

          

  <ul class="tabs">
    <li><a href="/aishek/crosspixel" class="selected" highlight="repo_sourcerepo_downloadsrepo_commitsrepo_tagsrepo_branches">Code</a></li>
    <li><a href="/aishek/crosspixel/network" highlight="repo_networkrepo_fork_queue">Network</a>
    <li><a href="/aishek/crosspixel/pulls" highlight="repo_pulls">Pull Requests <span class='counter'>0</span></a></li>

      <li><a href="/aishek/crosspixel/issues" highlight="repo_issues">Issues <span class='counter'>0</span></a></li>


    <li><a href="/aishek/crosspixel/graphs" highlight="repo_graphsrepo_contributors">Stats &amp; Graphs</a></li>

  </ul>

  
<div class="frame frame-center tree-finder" style="display:none"
      data-tree-list-url="/aishek/crosspixel/tree-list/829fd6f9f56c31ca76e8e06730aa8e1f1f277858"
      data-blob-url-prefix="/aishek/crosspixel/blob/829fd6f9f56c31ca76e8e06730aa8e1f1f277858"
    >

  <div class="breadcrumb">
    <b><a href="/aishek/crosspixel">crosspixel</a></b> /
    <input class="tree-finder-input js-navigation-enable" type="text" name="query" autocomplete="off" spellcheck="false">
  </div>

    <div class="octotip">
      <p>
        <a href="/aishek/crosspixel/dismiss-tree-finder-help" class="dismiss js-dismiss-tree-list-help" title="Hide this notice forever" rel="nofollow">Dismiss</a>
        <strong>Octotip:</strong> You've activated the <em>file finder</em>
        by pressing <span class="kbd">t</span> Start typing to filter the
        file list. Use <span class="kbd badmono">↑</span> and
        <span class="kbd badmono">↓</span> to navigate,
        <span class="kbd">enter</span> to view files.
      </p>
    </div>

  <table class="tree-browser" cellpadding="0" cellspacing="0">
    <tr class="js-header"><th>&nbsp;</th><th>name</th></tr>
    <tr class="js-no-results no-results" style="display: none">
      <th colspan="2">No matching files</th>
    </tr>
    <tbody class="js-results-list js-navigation-container">
    </tbody>
  </table>
</div>

<div id="jump-to-line" style="display:none">
  <h2>Jump to Line</h2>
  <form>
    <input class="textfield" type="text">
    <div class="full-button">
      <button type="submit" class="classy">
        <span>Go</span>
      </button>
    </div>
  </form>
</div>


<div class="subnav-bar">

  <ul class="actions subnav">
    <li><a href="/aishek/crosspixel/tags" class="blank" highlight="repo_tags">Tags <span class="counter">0</span></a></li>
    <li><a href="/aishek/crosspixel/downloads" class="" highlight="repo_downloads">Downloads <span class="counter">1</span></a></li>
    
  </ul>

  <ul class="scope">
    <li class="switcher">

      <div class="context-menu-container js-menu-container">
        <a href="#"
           class="minibutton bigger switcher js-menu-target js-commitish-button btn-branch repo-tree"
           data-master-branch="master"
           data-ref="master">
          <span><span class="icon"></span><i>branch:</i> master</span>
        </a>

        <div class="context-pane commitish-context js-menu-content">
          <a href="javascript:;" class="close js-menu-close"></a>
          <div class="context-title">Switch Branches/Tags</div>
          <div class="context-body pane-selector commitish-selector js-filterable-commitishes">
            <div class="filterbar">
              <div class="placeholder-field js-placeholder-field">
                <label class="placeholder" for="context-commitish-filter-field" data-placeholder-mode="sticky">Filter branches/tags</label>
                <input type="text" id="context-commitish-filter-field" class="commitish-filter" />
              </div>

              <ul class="tabs">
                <li><a href="#" data-filter="branches" class="selected">Branches</a></li>
                <li><a href="#" data-filter="tags">Tags</a></li>
              </ul>
            </div>

              <div class="commitish-item branch-commitish selector-item">
                <h4>
                    <a href="/aishek/crosspixel/blob/master/build/crosspixel.js" data-name="master" rel="nofollow">master</a>
                </h4>
              </div>


            <div class="no-results" style="display:none">Nothing to show</div>
          </div>
        </div><!-- /.commitish-context-context -->
      </div>

    </li>
  </ul>

  <ul class="subnav with-scope">

    <li><a href="/aishek/crosspixel" class="selected" highlight="repo_source">Files</a></li>
    <li><a href="/aishek/crosspixel/commits/master" highlight="repo_commits">Commits</a></li>
    <li><a href="/aishek/crosspixel/branches" class="" highlight="repo_branches" rel="nofollow">Branches <span class="counter">1</span></a></li>
  </ul>

</div>

  
  
  


          

        </div><!-- /.repohead -->

        




  
  <p class="last-commit">Latest commit to the <strong>master</strong> branch</p>

<div class="commit commit-tease js-details-container">
  <p class="commit-title ">
      <a href="/aishek/crosspixel/commit/829fd6f9f56c31ca76e8e06730aa8e1f1f277858" class="message">Fixed README typo</a>
      
  </p>
  <div class="commit-meta">
    <a href="/aishek/crosspixel/commit/829fd6f9f56c31ca76e8e06730aa8e1f1f277858" class="sha-block">commit <span class="sha">829fd6f9f5</span></a>
    <span class="js-clippy clippy-button " data-clipboard-text="829fd6f9f56c31ca76e8e06730aa8e1f1f277858" data-copied-hint="copied!" data-copy-hint="Copy SHA"></span>

    <div class="authorship">
      <img class="gravatar" height="20" src="https://secure.gravatar.com/avatar/4d09e19ac14ee6ae3bc86cea10dbe1fd?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-140.png" width="20" />
      <span class="author-name"><a href="/aishek">aishek</a></span>
      authored <time class="js-relative-date" datetime="2011-03-25T09:10:30-07:00" title="2011-03-25 09:10:30">March 25, 2011</time>

    </div>
  </div>
</div>


<!-- block_view_fragment_key: views4/v8/blob:v16:6f04339ecd0f61522dcf28069b71906a -->
  <div id="slider">

    <div class="breadcrumb" data-path="build/crosspixel.js/">
      <b><a href="/aishek/crosspixel/tree/829fd6f9f56c31ca76e8e06730aa8e1f1f277858" class="js-rewrite-sha">crosspixel</a></b> / <a href="/aishek/crosspixel/tree/829fd6f9f56c31ca76e8e06730aa8e1f1f277858/build" class="js-rewrite-sha">build</a> / crosspixel.js <span class="js-clippy clippy-button " data-clipboard-text="build/crosspixel.js" data-copied-hint="copied!" data-copy-hint="copy to clipboard"></span>
    </div>

    <div class="frames">
      <div class="frame frame-center" data-path="build/crosspixel.js/" data-permalink-url="/aishek/crosspixel/blob/829fd6f9f56c31ca76e8e06730aa8e1f1f277858/build/crosspixel.js" data-title="build/crosspixel.js at master from aishek/crosspixel - GitHub" data-type="blob">
          <ul class="big-actions">
            <li><a class="file-edit-link minibutton js-rewrite-sha" href="/aishek/crosspixel/edit/829fd6f9f56c31ca76e8e06730aa8e1f1f277858/build/crosspixel.js" data-method="post" rel="nofollow"><span>Edit this file</span></a></li>
          </ul>

        <div id="files" class="bubble">
          <div class="file">
            <div class="meta">
              <div class="info">
                <span class="icon"><img alt="Txt" height="16" src="https://a248.e.akamai.net/assets.github.com/images/icons/txt.png?1315867479" width="16" /></span>
                <span class="mode" title="File Mode">100644</span>
                  <span>45 lines (35 sloc)</span>
                <span>14.307 kb</span>
              </div>
              <ul class="actions">
                <li><a href="/aishek/crosspixel/raw/master/build/crosspixel.js" id="raw-url">raw</a></li>
                  <li><a href="/aishek/crosspixel/blame/master/build/crosspixel.js">blame</a></li>
                <li><a href="/aishek/crosspixel/commits/master/build/crosspixel.js" rel="nofollow">history</a></li>
              </ul>
            </div>
              <div class="data type-javascript">
      <table cellpadding="0" cellspacing="0" class="lines">
        <tr>
          <td>
            <pre class="line_numbers"><span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
</pre>
          </td>
          <td width="100%">
                <div class="highlight"><pre><div class='line' id='LC1'>var Crosspixel={};Crosspixel.Utils={};Crosspixel.Utils={documentBodyElement:null,getDocumentBodyElement:function(){if(this.documentBodyElement==null){this.documentBodyElement=document.getElementsByTagName(&quot;body&quot;)[0];}return this.documentBodyElement;},createParams:function(a,b){result=a;Crosspixel.Utils.mergeParams(a||{},b);return result;},mergeParams:function(a,c){for(var b in c){if(c.hasOwnProperty(b)){if(a[b]&amp;&amp;Crosspixel.Utils.isObject(a[b])){Crosspixel.Utils.mergeParams(a[b],c[b]);}else{a[b]=c[b];}}}},isObject:function(a){return Object.prototype.toString.call(a)===&quot;[object Object]&quot;;},defaultStyleValueParams:{display:&quot;block&quot;,width:&quot;100%&quot;,height:&quot;100%&quot;,opacity:1,background:&quot;transparent&quot;,&quot;float&quot;:&quot;none&quot;,visibility:&quot;visible&quot;,border:&quot;0&quot;},createStyleValue:function(f,d){var b=d||Crosspixel.Utils.defaultStyleValueParams;var e=Crosspixel.Utils.createParams(b,f);var a=&quot;&quot;;for(var c in e){if(e[c]||e[c]===0){a+=c+&quot;:&quot;+e[c]+&quot;;&quot;;}if(e[c]==&quot;opacity&quot;){a+=&quot;-khtml-opacity:&quot;+e[c]+&quot;;-moz-opacity:&quot;+e[c]+&quot;;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=&quot;+(e[c]*100)+&quot;);&quot;;}}return a;}};Crosspixel.Utils.EventProvider=function(a,c,b){this.eventName=a;this.prepareParams=c;this.target=b||&quot;document&quot;;this.handlers=null;return this;};Crosspixel.Utils.EventProvider.prototype.genericHandler=function(c){var d=(this.prepareParams?this.prepareParams(c):c);for(var a=0,b=this.handlers.length;a&lt;b;a++){this.handlers[a](d);}};Crosspixel.Utils.EventProvider.prototype.initHandlers=function(){this.handlers=[];var code=this.target+&quot;.on&quot;+this.eventName.toLowerCase()+&quot;=function(event){self.genericHandler(event);};&quot;;var self=this;eval(code);};Crosspixel.Utils.EventProvider.prototype.addHandler=function(a){if(this.handlers==null){this.initHandlers();}this.handlers[this.handlers.length]=a;};Crosspixel.Utils.EventSender=function(){this.handlers={};return this;};Crosspixel.Utils.EventSender.prototype.addHandler=function(a,b){if(!this.handlers[a]){this.handlers[a]=[];}this.handlers[a][this.handlers[a].length]=b;};Crosspixel.Utils.EventSender.prototype.occurEvent=function(a,e){var d=this.handlers[a];if(this.handlers[a]){for(var b=0,c=this.handlers[a].length;b&lt;c;b++){this.handlers[a][b](e);}}};Crosspixel.Utils.CookieStore={setValue:function(a,b){Crosspixel.Utils.CookieStore.setCookie(a,b);},getValue:function(a){return Crosspixel.Utils.CookieStore.getCookie(a);},setCookie:function(c,d){var b=new Date(),a=new Date();a.setTime(b.getTime()+31536000000);document.cookie=c+&quot;=&quot;+escape(d)+&quot;; expires=&quot;+a;},getCookie:function(b){var d=&quot; &quot;+document.cookie;var c=&quot; &quot;+b+&quot;=&quot;;var e=null;var f=0;var a=0;if(d.length&gt;0){f=d.indexOf(c);if(f!=-1){f+=c.length;a=d.indexOf(&quot;;&quot;,f);if(a==-1){a=d.length;}e=unescape(d.substring(f,a));}}return(e);}};Crosspixel.Utils.StateChanger=function(c,b,a){c.addHandler(function(d){if(b(d)){a();}});return this;};Crosspixel.OpacityChanger={};Crosspixel.OpacityChanger={params:null,eventSender:null,init:function(a){this.params=Crosspixel.Utils.createParams(this.defaults,a);this.eventSender=new Crosspixel.Utils.EventSender();},setOpacity:function(a){this.params.opacity=a;this.params.opacity=(this.params.opacity&lt;0?0:this.params.opacity);this.params.opacity=(this.params.opacity&gt;1?1:this.params.opacity);this.updateOpacity(this.params.opacity);return this.params.opacity;},stepDownOpacity:function(){return this.setOpacity(this.params.opacity-this.params.opacityStep);},stepUpOpacity:function(){return this.setOpacity(this.params.opacity+this.params.opacityStep);},updateOpacity:function(a){this.eventSender.occurEvent(&quot;opacityChanged&quot;,this.params.opacity);},changeElementOpacity:function(a){if(a){a.style.opacity=this.params.opacity;}}};Crosspixel.OpacityChanger.defaults={shouldStepUpOpacity:function(b){var a=!b.occured_in_form&amp;&amp;(b.ctrlKey&amp;&amp;(b.character==&quot;o&quot;||b.character==&quot;O&quot;||b.character==&quot;щ&quot;||b.character==&quot;Щ&quot;));return a;},shouldStepDownOpacity:function(b){var a=!b.occured_in_form&amp;&amp;(b.ctrlKey&amp;&amp;(b.character==&quot;u&quot;||b.character==&quot;U&quot;||b.character==&quot;г&quot;||b.character==&quot;Г&quot;));return a;},opacity:0.25,opacityStep:0.05};Crosspixel.Image={};Crosspixel.Image={showing:false,parentElement:null,params:null,imgElement:null,eventSender:null,init:function(a){this.params=Crosspixel.Utils.createParams(this.defaults,a);this.eventSender=new Crosspixel.Utils.EventSender();},createParentElement:function(c){var b=document.createElement(&quot;div&quot;);var a={position:&quot;absolute&quot;,left:&quot;0&quot;,top:&quot;0&quot;,width:&quot;100%&quot;,height:c.height+&quot;px&quot;,opacity:1,&quot;z-index&quot;:c[&quot;z-index&quot;]};b.setAttribute(&quot;style&quot;,Crosspixel.Utils.createStyleValue(a));b.appendChild(this.createImageDOM(c));Crosspixel.Utils.getDocumentBodyElement().appendChild(b);return b;},createImageDOM:function(d){var a={position:&quot;static&quot;,width:&quot;auto&quot;,height:&quot;auto&quot;,opacity:Crosspixel.OpacityChanger.params.opacity};var c={position:&quot;static&quot;,&quot;padding-top&quot;:d[&quot;margin-top&quot;],width:&quot;auto&quot;,height:&quot;auto&quot;};if(d.centered){c[&quot;text-align&quot;]=&quot;center&quot;;a.margin=&quot;0 auto&quot;;}else{c[&quot;padding-left&quot;]=d[&quot;margin-left&quot;],c[&quot;padding-right&quot;]=d[&quot;margin-right&quot;];}var b=document.createElement(&quot;div&quot;);b.setAttribute(&quot;style&quot;,Crosspixel.Utils.createStyleValue(c));this.imgElement=document.createElement(&quot;img&quot;);this.imgElement.setAttribute(&quot;src&quot;,d.src);this.imgElement.setAttribute(&quot;width&quot;,d.width);this.imgElement.setAttribute(&quot;height&quot;,d.height);this.imgElement.setAttribute(&quot;style&quot;,Crosspixel.Utils.createStyleValue(a));b.appendChild(this.imgElement);return b;},opacityHandler:function(){Crosspixel.OpacityChanger.changeElementOpacity(Crosspixel.Image.imgElement);},toggleVisibility:function(){this.showing=!this.showing;this.eventSender.occurEvent(&quot;visibilityChanged&quot;,this.showing);if(this.showing&amp;&amp;this.parentElement==null){this.parentElement=this.createParentElement(this.params);}if(this.parentElement){this.parentElement.style.display=(this.showing?&quot;block&quot;:&quot;none&quot;);}}};Crosspixel.Image.defaults={shouldToggleVisibility:function(b){var a=!b.occured_in_form&amp;&amp;(b.ctrlKey&amp;&amp;(b.character==&quot;i&quot;||b.character==&quot;I&quot;||b.character==&quot;ш&quot;||b.character==&quot;Ш&quot;));return a;},&quot;z-index&quot;:255,centered:false,&quot;margin-top&quot;:0,&quot;margin-left&quot;:&quot;0px&quot;,&quot;margin-right&quot;:&quot;0px&quot;,src:&quot;&quot;,width:100,height:100};Crosspixel.Resizer={};Crosspixel.Resizer={params:null,sizes:null,currentSizeIndex:null,title:null,eventSender:null,detectDefaultSize:function(){var a=null;if(typeof(window.innerWidth)==&quot;number&quot;&amp;&amp;typeof(window.innerHeight)==&quot;number&quot;){a={width:window.innerWidth,height:window.innerHeight};}else{if(document.documentElement&amp;&amp;(document.documentElement.clientWidth||document.documentElement.clientHeight)){a={width:document.documentElement.clientWidth,height:document.documentElement.clientHeight};}else{if(document.body&amp;&amp;(document.body.clientWidth||document.body.clientHeight)){a={width:document.body.clientWidth,height:document.body.clientHeight};}}}return a;},getDefaultSize:function(){return this.sizes[0];},getCurrentSize:function(){return this.sizes[this.currentSizeIndex];},init:function(c){this.params=Crosspixel.Utils.createParams(this.defaults,{});this.eventSender=new Crosspixel.Utils.EventSender();var b=this.detectDefaultSize();if(b){var a=[b];a[a.length]={width:c.width,height:c.height};this.title=document.title;this.sizes=a;this.currentSizeIndex=0;}},sizeTitle:function(d){var b,f=this.sizes[d],c=this.sizes[0];if(f.title){b=f.title;}else{var e=(f.width?f.width:c.width);var a=(f.height?f.height:c.height);b=e+&quot;×&quot;+a;}return b;},selectSize:function(a){this.currentSizeIndex=a;this.applySize();},toggleSize:function(){if(this.currentSizeIndex!=null){this.currentSizeIndex++;this.currentSizeIndex=(this.currentSizeIndex==this.sizes.length?0:this.currentSizeIndex);this.applySize();}},applySize:function(){var c=(this.getCurrentSize().width?this.getCurrentSize().width:this.getDefaultSize().width);var a=(this.getCurrentSize().height?this.getCurrentSize().height:this.getDefaultSize().height);window.resizeTo(c,a);var b=(this.currentSizeIndex?this.title+&quot; (&quot;+c+&quot;×&quot;+a+&quot;)&quot;:this.title);if(this.getCurrentSize().title){b=this.getCurrentSize().title;}document.title=b;this.eventSender.occurEvent(&quot;sizeChanged&quot;,this.currentSizeIndex);}};Crosspixel.GUI={params:null,togglerElement:null,paneElement:null,paneShowing:true,checkboxes:{},init:function(a){this.params=Crosspixel.Utils.createParams(this.defaults,a);},create:function(){this.createToggler();this.createPane();},createToggler:function(){var b=this;b.togglerElement=document.createElement(&quot;button&quot;);b.togglerElement.innerHTML=b.params.toggler.label;var a=Crosspixel.Utils.createStyleValue(b.params.toggler.style,{});b.togglerElement.setAttribute(&quot;style&quot;,a);Crosspixel.Utils.getDocumentBodyElement().appendChild(b.togglerElement);b.togglerElement.onclick=function(){b.paneShowing=!b.paneShowing;b.paneElement.style.display=(b.paneShowing?&quot;block&quot;:&quot;none&quot;);};},createPaneCheckboxItemHTML:function(e,b,d){var a=d||&quot;&quot;;var c='&lt;div style=&quot;width:auto;'+a+'&quot;&gt;';c+='&lt;input type=&quot;checkbox&quot; id=&quot;'+e+'&quot;&gt;';c+='&lt;label for=&quot;'+e+'&quot;&gt;&amp;nbsp;'+b+&quot;&lt;/label&gt;&quot;;c+=&quot;&lt;/div&gt;&quot;;return c;},createPane:function(){var b=this;b.paneElement=document.createElement(&quot;div&quot;);var c=b.params.pane.style;var a=Crosspixel.Utils.createStyleValue(c,{});b.paneElement.setAttribute(&quot;style&quot;,a);var e={},d=&quot;&quot;;e.image=b.generateId()+&quot;image&quot;;d+=b.createPaneCheckboxItemHTML(e.image,b.params.pane.labels.image,&quot;margin:0 0 1em&quot;);d+='&lt;div style=&quot;width:auto;margin:1em 0 0&quot;&gt;';e.opacity_down=b.generateId()+&quot;opacitydown&quot;;e.opacity_up=b.generateId()+&quot;opacityup&quot;;e.opacity_value=b.generateId()+&quot;opacityvalue&quot;;if(b.params.pane.labels.opacity){d+=b.params.pane.labels.opacity.label+&quot;&lt;br&gt;&quot;;}d+=b.params.pane.labels.opacity.less;d+='&lt;button id=&quot;'+e.opacity_down+'&quot;&gt;-&lt;/button&gt;&amp;nbsp;';d+='&lt;span id=&quot;'+e.opacity_value+'&quot;&gt;'+Crosspixel.OpacityChanger.params.opacity.toFixed(2)+&quot;&lt;/span&gt;&quot;;d+='&amp;nbsp;&lt;button id=&quot;'+e.opacity_up+'&quot;&gt;+&lt;/button&gt;';d+=b.params.pane.labels.opacity.more;d+=&quot;&lt;/div&gt;&quot;;b.paneElement.innerHTML=d;Crosspixel.Utils.getDocumentBodyElement().appendChild(this.paneElement);b.checkboxes.image=document.getElementById(e.image);if(b.checkboxes.image){b.checkboxes.image.onclick=function(){Crosspixel.Image.toggleVisibility();};Crosspixel.Image.eventSender.addHandler(&quot;visibilityChanged&quot;,function(f){b.checkboxes.image.checked=f;});}b.checkboxes.opacity_value=document.getElementById(e.opacity_value);if(b.checkboxes.opacity_value){Crosspixel.OpacityChanger.eventSender.addHandler(&quot;opacityChanged&quot;,function(f){b.checkboxes.opacity_value.innerHTML=f.toFixed(2);});}b.checkboxes.opacity_up=document.getElementById(e.opacity_up);if(b.checkboxes.opacity_up){b.checkboxes.opacity_up.onclick=function(){Crosspixel.OpacityChanger.stepUpOpacity();};}b.checkboxes.opacity_down=document.getElementById(e.opacity_down);if(b.checkboxes.opacity_down){b.checkboxes.opacity_down.onclick=function(){Crosspixel.OpacityChanger.stepDownOpacity();};}},generateId:function(){var b=&quot;_mdg&quot;,a=new Date();a=b+a.getTime();return a;}};Crosspixel.GUI.defaults={toggler:{style:{position:&quot;absolute&quot;,right:&quot;10px&quot;,top:&quot;10px&quot;,&quot;z-index&quot;:1000},label:&quot;Настройки&quot;},pane:{style:{position:&quot;absolute&quot;,right:&quot;10px&quot;,top:&quot;35px&quot;,width:&quot;auto&quot;,height:&quot;auto&quot;,margin:&quot;0&quot;,padding:&quot;7px 5px&quot;,background:&quot;#FFF&quot;,border:&quot;2px solid #CCC&quot;,&quot;z-index&quot;:1000},labels:{image:'изображение-макет &lt;span style=&quot;color:#555;font-size:80%;margin-left:0.75em&quot;&gt;Ctrl i&lt;/span&gt;',opacity:{label:'&lt;span style=&quot;margin-left:3.7em&quot;&gt;прозрачность&lt;/span&gt;',less:'&lt;span style=&quot;color:#555;font-size:80%;margin:0 0.75em 0 1em&quot;&gt;Ctrl u&lt;/span&gt; ',more:' &lt;span style=&quot;color:#555;font-size:80%;margin-left:0.75em&quot;&gt;Ctrl o&lt;/span&gt;'}}}};Crosspixel.keyDownEventProvider=null;Crosspixel.resizeEventProvider=null;Crosspixel.getResizeEventProvider=function(){if(this.resizeEventProvider==null){this.resizeEventProvider=new Crosspixel.Utils.EventProvider(&quot;resize&quot;,function(a){return{event:a};},&quot;window&quot;);}return this.resizeEventProvider;};Crosspixel.getKeyDownEventProvider=function(){if(this.keyDownEventProvider==null){this.keyDownEventProvider=new Crosspixel.Utils.EventProvider(&quot;keydown&quot;,function(c){var b=(c||window.event);var e=(b.keyCode?b.keyCode:(b.which?b.which:b.keyChar));var d=String.fromCharCode(e).toLowerCase();var f={&quot;`&quot;:&quot;~&quot;,&quot;1&quot;:&quot;!&quot;,&quot;2&quot;:&quot;@&quot;,&quot;3&quot;:&quot;#&quot;,&quot;4&quot;:&quot;$&quot;,&quot;5&quot;:&quot;%&quot;,&quot;6&quot;:&quot;^&quot;,&quot;7&quot;:&quot;&amp;&quot;,&quot;8&quot;:&quot;*&quot;,&quot;9&quot;:&quot;(&quot;,&quot;0&quot;:&quot;)&quot;,&quot;-&quot;:&quot;_&quot;,&quot;=&quot;:&quot;+&quot;,&quot;;&quot;:&quot;:&quot;,&quot;'&quot;:'&quot;',&quot;,&quot;:&quot;&lt;&quot;,&quot;.&quot;:&quot;&gt;&quot;,&quot;/&quot;:&quot;?&quot;,&quot;\\&quot;:&quot;|&quot;};if(b.shiftKey&amp;&amp;f[d]){d=f[d];}var a=(b.target?b.target:b.srcElement);if(a&amp;&amp;a.nodeType==3){a=a.parentNode;}var g=(a&amp;&amp;(a.tagName==&quot;INPUT&quot;||a.tagName==&quot;TEXTAREA&quot;));return{occured_in_form:g,character:d,keyCode:e,altKey:b.altKey,shiftKey:b.shiftKey,ctrlKey:b.ctrlKey,event:b};});}return this.keyDownEventProvider;};Crosspixel.init=function(g){var b=this;var a=Crosspixel.Utils.CookieStore;this.OpacityChanger.init(g.opacity);var e=new Crosspixel.Utils.StateChanger(this.getKeyDownEventProvider(),this.OpacityChanger.params.shouldStepUpOpacity,function(){if(!Crosspixel.Image.showing){Crosspixel.Image.toggleVisibility();}b.OpacityChanger.stepUpOpacity();});var d=new Crosspixel.Utils.StateChanger(this.getKeyDownEventProvider(),this.OpacityChanger.params.shouldStepDownOpacity,function(){if(!Crosspixel.Image.showing){Crosspixel.Image.toggleVisibility();}b.OpacityChanger.stepDownOpacity();});this.OpacityChanger.eventSender.addHandler(&quot;opacityChanged&quot;,function(h){a.setValue(&quot;o&quot;,h);});this.Image.init(g.image);this.OpacityChanger.eventSender.addHandler(&quot;opacityChanged&quot;,this.Image.opacityHandler);var c=new Crosspixel.Utils.StateChanger(this.getKeyDownEventProvider(),this.Image.params.shouldToggleVisibility,function(){b.Image.toggleVisibility();});this.Image.eventSender.addHandler(&quot;visibilityChanged&quot;,function(i){b.Resizer.toggleSize();var h=(i?&quot;true&quot;:&quot;false&quot;);a.setValue(&quot;i&quot;,h);});b.Resizer.init(this.Image.params);b.GUI.init(g.gui);b.GUI.create();if(a.getValue(&quot;i&quot;)==&quot;true&quot;){b.Image.toggleVisibility();}var f=parseFloat(a.getValue(&quot;o&quot;));if(!isNaN(f)){b.OpacityChanger.setOpacity(f);}};/** @include &quot;index.js&quot; */</div><div class='line' id='LC2'><br/></div><div class='line' id='LC3'>Crosspixel.init(</div><div class='line' id='LC4'>	{</div><div class='line' id='LC5'><br/></div><div class='line' id='LC6'>		image: {</div><div class='line' id='LC7'>			'z-index': 255,</div><div class='line' id='LC8'><br/></div><div class='line' id='LC9'>			centered: true,</div><div class='line' id='LC10'><br/></div><div class='line' id='LC11'>			'margin-top': '0px',</div><div class='line' id='LC12'>			'margin-left': '0px',</div><div class='line' id='LC13'>			'margin-right': '0px',</div><div class='line' id='LC14'><br/></div><div class='line' id='LC15'>			src: 'design.png',</div><div class='line' id='LC16'><br/></div><div class='line' id='LC17'>			width: 300,</div><div class='line' id='LC18'>			height: 356</div><div class='line' id='LC19'>		},</div><div class='line' id='LC20'><br/></div><div class='line' id='LC21'>		opacity: {</div><div class='line' id='LC22'>			opacity: 1,</div><div class='line' id='LC23'>			opacityStep: 0.05</div><div class='line' id='LC24'>		},</div><div class='line' id='LC25'><br/></div><div class='line' id='LC26'>		gui: {</div><div class='line' id='LC27'>			toggler: {</div><div class='line' id='LC28'>				style: {</div><div class='line' id='LC29'>					position: &quot;absolute&quot;,</div><div class='line' id='LC30'>					right: '10px',</div><div class='line' id='LC31'>					top: '10px'</div><div class='line' id='LC32'>				}</div><div class='line' id='LC33'>			},</div><div class='line' id='LC34'><br/></div><div class='line' id='LC35'>			pane: {</div><div class='line' id='LC36'>				style: {</div><div class='line' id='LC37'>					position: &quot;absolute&quot;,</div><div class='line' id='LC38'>					right: '10px',</div><div class='line' id='LC39'>					top: '35px'</div><div class='line' id='LC40'>				}</div><div class='line' id='LC41'>			}</div><div class='line' id='LC42'>		}</div><div class='line' id='LC43'><br/></div><div class='line' id='LC44'>	}</div><div class='line' id='LC45'>);</div></pre></div>
          </td>
        </tr>
      </table>
  </div>

          </div>
        </div>
      </div>
    </div>

  </div>

<div class="frame frame-loading" style="display:none;" data-tree-list-url="/aishek/crosspixel/tree-list/829fd6f9f56c31ca76e8e06730aa8e1f1f277858" data-blob-url-prefix="/aishek/crosspixel/blob/829fd6f9f56c31ca76e8e06730aa8e1f1f277858">
  <img src="https://a248.e.akamai.net/assets.github.com/images/modules/ajax/big_spinner_336699.gif?1315867479" height="32" width="32">
</div>

      </div>
      <div class="context-overlay"></div>
    </div>


      <!-- footer -->
      <div id="footer" >
        
  <div class="upper_footer">
     <div class="container clearfix">

       <!--[if IE]><h4 id="blacktocat_ie">GitHub Links</h4><![endif]-->
       <![if !IE]><h4 id="blacktocat">GitHub Links</h4><![endif]>

       <ul class="footer_nav">
         <h4>GitHub</h4>
         <li><a href="https://github.com/about">About</a></li>
         <li><a href="https://github.com/blog">Blog</a></li>
         <li><a href="https://github.com/features">Features</a></li>
         <li><a href="https://github.com/contact">Contact &amp; Support</a></li>
         <li><a href="https://github.com/training">Training</a></li>
         <li><a href="http://enterprise.github.com/">GitHub Enterprise</a></li>
         <li><a href="http://status.github.com/">Site Status</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Tools</h4>
         <li><a href="http://get.gaug.es/">Gauges: Analyze web traffic</a></li>
         <li><a href="http://speakerdeck.com">Speaker Deck: Presentations</a></li>
         <li><a href="https://gist.github.com">Gist: Code snippets</a></li>
         <li><a href="http://mac.github.com/">GitHub for Mac</a></li>
         <li><a href="http://mobile.github.com/">Issues for iPhone</a></li>
         <li><a href="http://jobs.github.com/">Job Board</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Extras</h4>
         <li><a href="http://shop.github.com/">GitHub Shop</a></li>
         <li><a href="http://octodex.github.com/">The Octodex</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Documentation</h4>
         <li><a href="http://help.github.com/">GitHub Help</a></li>
         <li><a href="http://developer.github.com/">Developer API</a></li>
         <li><a href="http://github.github.com/github-flavored-markdown/">GitHub Flavored Markdown</a></li>
         <li><a href="http://pages.github.com/">GitHub Pages</a></li>
       </ul>

     </div><!-- /.site -->
  </div><!-- /.upper_footer -->

<div class="lower_footer">
  <div class="container clearfix">
    <!--[if IE]><div id="legal_ie"><![endif]-->
    <![if !IE]><div id="legal"><![endif]>
      <ul>
          <li><a href="https://github.com/site/terms">Terms of Service</a></li>
          <li><a href="https://github.com/site/privacy">Privacy</a></li>
          <li><a href="https://github.com/security">Security</a></li>
      </ul>

      <p>&copy; 2012 <span title="0.24332s from fe9.rs.github.com">GitHub</span> Inc. All rights reserved.</p>
    </div><!-- /#legal or /#legal_ie-->

      <div class="sponsor">
        <a href="http://www.rackspace.com" class="logo">
          <img alt="Dedicated Server" height="36" src="https://a248.e.akamai.net/assets.github.com/images/modules/footer/rackspace_logo.png?v2" width="38" />
        </a>
        Powered by the <a href="http://www.rackspace.com ">Dedicated
        Servers</a> and<br/> <a href="http://www.rackspacecloud.com">Cloud
        Computing</a> of Rackspace Hosting<span>&reg;</span>
      </div>
  </div><!-- /.site -->
</div><!-- /.lower_footer -->

      </div><!-- /#footer -->

    

<div id="keyboard_shortcuts_pane" class="instapaper_ignore readability-extra" style="display:none">
  <h2>Keyboard Shortcuts <small><a href="#" class="js-see-all-keyboard-shortcuts">(see all)</a></small></h2>

  <div class="columns threecols">
    <div class="column first">
      <h3>Site wide shortcuts</h3>
      <dl class="keyboard-mappings">
        <dt>s</dt>
        <dd>Focus site search</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>?</dt>
        <dd>Bring up this help dialog</dd>
      </dl>
    </div><!-- /.column.first -->

    <div class="column middle" style='display:none'>
      <h3>Commit list</h3>
      <dl class="keyboard-mappings">
        <dt>j</dt>
        <dd>Move selection down</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>k</dt>
        <dd>Move selection up</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>c <em>or</em> o <em>or</em> enter</dt>
        <dd>Open commit</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>y</dt>
        <dd>Expand URL to its canonical form</dd>
      </dl>
    </div><!-- /.column.first -->

    <div class="column last" style='display:none'>
      <h3>Pull request list</h3>
      <dl class="keyboard-mappings">
        <dt>j</dt>
        <dd>Move selection down</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>k</dt>
        <dd>Move selection up</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>o <em>or</em> enter</dt>
        <dd>Open issue</dd>
      </dl>
    </div><!-- /.columns.last -->

  </div><!-- /.columns.equacols -->

  <div style='display:none'>
    <div class="rule"></div>

    <h3>Issues</h3>

    <div class="columns threecols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt>j</dt>
          <dd>Move selection down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>k</dt>
          <dd>Move selection up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>x</dt>
          <dd>Toggle selection</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o <em>or</em> enter</dt>
          <dd>Open issue</dd>
        </dl>
      </div><!-- /.column.first -->
      <div class="column middle">
        <dl class="keyboard-mappings">
          <dt>I</dt>
          <dd>Mark selection as read</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>U</dt>
          <dd>Mark selection as unread</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>e</dt>
          <dd>Close selection</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>y</dt>
          <dd>Remove selection from view</dd>
        </dl>
      </div><!-- /.column.middle -->
      <div class="column last">
        <dl class="keyboard-mappings">
          <dt>c</dt>
          <dd>Create issue</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>l</dt>
          <dd>Create label</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>i</dt>
          <dd>Back to inbox</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>u</dt>
          <dd>Back to issues</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>/</dt>
          <dd>Focus issues search</dd>
        </dl>
      </div>
    </div>
  </div>

  <div style='display:none'>
    <div class="rule"></div>

    <h3>Issues Dashboard</h3>

    <div class="columns threecols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt>j</dt>
          <dd>Move selection down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>k</dt>
          <dd>Move selection up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o <em>or</em> enter</dt>
          <dd>Open issue</dd>
        </dl>
      </div><!-- /.column.first -->
    </div>
  </div>

  <div style='display:none'>
    <div class="rule"></div>

    <h3>Network Graph</h3>
    <div class="columns equacols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt><span class="badmono">←</span> <em>or</em> h</dt>
          <dd>Scroll left</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">→</span> <em>or</em> l</dt>
          <dd>Scroll right</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">↑</span> <em>or</em> k</dt>
          <dd>Scroll up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">↓</span> <em>or</em> j</dt>
          <dd>Scroll down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>t</dt>
          <dd>Toggle visibility of head labels</dd>
        </dl>
      </div><!-- /.column.first -->
      <div class="column last">
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">←</span> <em>or</em> shift h</dt>
          <dd>Scroll all the way left</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">→</span> <em>or</em> shift l</dt>
          <dd>Scroll all the way right</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">↑</span> <em>or</em> shift k</dt>
          <dd>Scroll all the way up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">↓</span> <em>or</em> shift j</dt>
          <dd>Scroll all the way down</dd>
        </dl>
      </div><!-- /.column.last -->
    </div>
  </div>

  <div >
    <div class="rule"></div>
    <div class="columns threecols">
      <div class="column first" >
        <h3>Source Code Browsing</h3>
        <dl class="keyboard-mappings">
          <dt>t</dt>
          <dd>Activates the file finder</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>l</dt>
          <dd>Jump to line</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>w</dt>
          <dd>Switch branch/tag</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>y</dt>
          <dd>Expand URL to its canonical form</dd>
        </dl>
      </div>
    </div>
  </div>
</div>

    <div id="markdown-help" class="instapaper_ignore readability-extra">
  <h2>Markdown Cheat Sheet</h2>

  <div class="cheatsheet-content">

  <div class="mod">
    <div class="col">
      <h3>Format Text</h3>
      <p>Headers</p>
      <pre>
# This is an &lt;h1&gt; tag
## This is an &lt;h2&gt; tag
###### This is an &lt;h6&gt; tag</pre>
     <p>Text styles</p>
     <pre>
*This text will be italic*
_This will also be italic_
**This text will be bold**
__This will also be bold__

*You **can** combine them*
</pre>
    </div>
    <div class="col">
      <h3>Lists</h3>
      <p>Unordered</p>
      <pre>
* Item 1
* Item 2
  * Item 2a
  * Item 2b</pre>
     <p>Ordered</p>
     <pre>
1. Item 1
2. Item 2
3. Item 3
   * Item 3a
   * Item 3b</pre>
    </div>
    <div class="col">
      <h3>Miscellaneous</h3>
      <p>Images</p>
      <pre>
![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)
</pre>
     <p>Links</p>
     <pre>
http://github.com - automatic!
[GitHub](http://github.com)</pre>
<p>Blockquotes</p>
     <pre>
As Kanye West said:

> We're living the future so
> the present is our past.
</pre>
    </div>
  </div>
  <div class="rule"></div>

  <h3>Code Examples in Markdown</h3>
  <div class="col">
      <p>Syntax highlighting with <a href="http://github.github.com/github-flavored-markdown/" title="GitHub Flavored Markdown" target="_blank">GFM</a></p>
      <pre>
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```</pre>
    </div>
    <div class="col">
      <p>Or, indent your code 4 spaces</p>
      <pre>
Here is a Python code example
without syntax highlighting:

    def foo:
      if not bar:
        return true</pre>
    </div>
    <div class="col">
      <p>Inline code for comments</p>
      <pre>
I think you should use an
`&lt;addr&gt;` element here instead.</pre>
    </div>
  </div>

  </div>
</div>


    <div class="ajax-error-message">
      <p><span class="icon"></span> Something went wrong with that request. Please try again. <a href="javascript:;" class="ajax-error-dismiss">Dismiss</a></p>
    </div>

    
    
    
    <span id='server_response_time' data-time='0.24474' data-host='fe9'></span>
  </body>
</html>

