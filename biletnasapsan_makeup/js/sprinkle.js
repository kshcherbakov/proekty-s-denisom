$(document).ready(function() {

    // здесь любым удобным способом инициализировать переменную data и serviceClass
	// data  - данные о свободных местах
	// serviceClass - класс обслуживания,принимает значения biznes или econom
	var serviceClass = 'all';
	var data = [
				{CarNum:1, Price: 10990, Type: "Бизнес-класс", places:[1,4,7,15]},
				{CarNum:2, Price: 10990, Type: "Бизнес-класс", places:[]},
				{CarNum:3, Price: 6870, Type: "Эконом-класс", places:[21,24,27,35]},
				{CarNum:4, Price: 6870, Type: "Эконом-класс", places:[31,34,37,41]},
				{CarNum:6, Price: 6870, Type: "Эконом-класс", places:[10,11,12,13,14,15,16,17,18,19,20]},
				{CarNum:7, Price: 6870, Type: "Эконом-класс", places:[10,11,12,13,14,15,16,17,18,19,20]},
				{CarNum:8, Price: 6870, Type: "Эконом-класс", places:[10,11,12,13,14,15,16,17,18,19,20]},
				{CarNum:9, Price: 6870, Type: "Эконом-класс", places:[10,11,12,13,14,15,16,17,18,19,20]},
				{CarNum:10, Price: 6870, Type: "Эконом-класс", places:[10,11,12,13,14,15,16,17,18,19,20]}
			   ];

	$('#tabvanilla > ul > li > a').each( function(){
		if( $(this).attr('href') == '#' + serviceClass) {
			$(this).parent().addClass('ui-tabs-selected');
		}
	})
	
    $('#tabvanilla > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle'} });
    $('#featuredvid > ul').tabs();

    $('div[class^="wagon"] .car-place, div[class^="wagon"] .car-placer').click(function (event) {
        var places = $(this).closest('.show, .hide').prev().places('getReserved');
        if (places.length >= 4 && $(this).attr('state') != 1) {
            alert('По правилам РЖД, вы не можете купить более четырех билетов в рамках одной транзакции.');
            event.stopImmediatePropagation();
        }
    });

			   
	$.each(data, function(index, obj){
		var target = $('div.wagon' + obj.CarNum).parent().prev();
		var targetPlaces = obj.places;
		target.places({
                freePlaces: targetPlaces,
				cartNumber: obj.CarNum,
				price: obj.Price,
				type: obj.Type
            });
	});
	//обработчик клика на "Указать пассажиров" - возвращает искомый JSON
	$('a#aSubmit').click(function() {
		$('.tooltip').fadeOut(1000);
		var expanded = $('#all h2.expanded, #popular h2.expanded, #recent h2.expanded');
        if (expanded.places('getReserved').length > 0) {
			var res = {CarNum: expanded.places('getCar'), Price: expanded.places('getPrice'), Type: expanded.places('getType'), places: expanded.places('getReserved')};
			return res;
		}
	});
	
	
	$('#biznes h2, #econom h2, #all h2').click(function (event) {
		$('.tooltip').fadeOut(1000);
        var expanded = $('#biznes h2.expanded, #econom h2.expanded, #all h2.expanded');
        if (expanded.places('getReserved').length > 0) {
            if (confirm('При закрытии вкладки выбранные Вами места освободятся. Вы действительно хотите закрыть вкладку?')) {
                expanded.places('clearReserved');
            } else {
                event.stopImmediatePropagation();
            }
        }
    });
    
    $('ul.ui-tabs-nav a').click(function(event) {
		$('.tooltip').fadeOut(1000);
        var expanded = $('#biznes h2.expanded, #econom h2.expanded, #all h2.expanded');
        if (expanded.places('getReserved').length > 0) {
            if (confirm('При закрытии вкладки выбранные Вами места освободятся. Вы действительно хотите закрыть вкладку?')) {
                expanded.places('clearReserved');
            } else {
                event.stopImmediatePropagation();
            }
        }
    });
});


