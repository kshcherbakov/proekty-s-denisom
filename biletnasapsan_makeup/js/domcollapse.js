/*  
 * DOMcollapse
 * Version 3.0
 * released 06.12.2005 
 * Not for commercial reselling or use, unless consent given by the author
 * Check for updates on http://onlinetools.org and http://wait-till-i.com
 *
*/

dc = {
    triggerElements: '*', 	// elements to trigger the effect
    parentElementId: null,	// ID of the parent element (keep null if none)
    uniqueCollapse: true,	// is set to true only one element can be open at a time

    // CSS class names
    trigger: 'trigger',
    triggeropen: 'expanded',
    hideClass: 'hide',
    showClass: 'show',

    // pictures and text alternatives
    closedPic: 'css/images/opn.png',
    closedAlt: 'Выбрать места',
    openPic: 'css/images/clos.png',
    openAlt: 'Отменить выбор мест',

    init: function(e) {
        var temp;
        if (!document.getElementById || !document.createTextNode) {
            return;
        }
        if (!dc.parentElementId) {
            temp = document.getElementsByTagName(dc.triggerElements);
        } else if (document.getElementById(dc.parentElementId)) {
            temp = document.getElementById(dc.parentElementId).getElementsByTagName(dc.triggerElements);
        } else {
            return;
        }
        dc.tempLink = document.createElement('a');
        dc.tempLink.setAttribute('href', '#');
        dc.tempLink.appendChild(document.createElement('img'));
        for (var i = 0; i < temp.length; i++) {
            if (dc.cssjs('check', temp[i], dc.trigger) || dc.cssjs('check', temp[i], dc.triggeropen)) {
                dc.makeTrigger(temp[i], e);
            }
        }
    },
    makeTrigger: function(o, e) {
        var tl = dc.tempLink.cloneNode(true);
        var tohide = o.nextSibling;
        while (tohide.nodeType != 1) {
            tohide = tohide.nextSibling;
        }
        o.tohide = tohide;
        if (!dc.cssjs('check', o, dc.triggeropen)) {
            dc.cssjs('add', tohide, dc.hideClass);
            tl.getElementsByTagName('img')[0].setAttribute('src', dc.closedPic);
            tl.getElementsByTagName('img')[0].setAttribute('alt', dc.closedAlt);
            o.setAttribute('title', dc.closedAlt);
        } else {
            dc.cssjs('add', tohide, dc.showClass);
            tl.getElementsByTagName('img')[0].setAttribute('src', dc.openPic);
            tl.getElementsByTagName('img')[0].setAttribute('alt', dc.openAlt);
            o.setAttribute('title', dc.openAlt);
            dc.currentOpen = o;
        }
        $(o).click(dc.addCollapse);
        o.insertBefore(tl, o.firstChild);
        $(tl).click(dc.addCollapse);
    },
    addCollapse: function(e) {
        var action, pic;
        // hack to fix safari's redraw bug 
        // as mentioned on http://en.wikipedia.org/wiki/Wikipedia:Browser_notes#Mac_OS_X
        if (self.screenTop && self.screenX) {
            window.resizeTo(self.outerWidth + 1, self.outerHeight);
            window.resizeTo(self.outerWidth - 1, self.outerHeight);
        }

        var toggleLine = function(line, setVisible) {
            $(line).attr('title', setVisible ? dc.openAlt : dc.closedAlt)
                .find('img').eq(0).attr({
                    src: setVisible ? dc.openPic : dc.closedPic,
                    alt: setVisible ? dc.openAlt : dc.closedAlt
                });

            $(line.tohide).toggleClass(dc.hideClass + ' ' + dc.showClass);
            $(line).toggleClass(dc.triggeropen, setVisible).toggleClass(dc.trigger, setVisible);
        }

        var o = this; //dc.getTarget(e);		

        if (o.tohide) {

            var isHidden = $(o.tohide).hasClass(dc.hideClass);
            toggleLine(o, isHidden);

            if (dc.uniqueCollapse && dc.currentOpen && dc.currentOpen != dc.getTarget(e) && $(dc.currentOpen.tohide).hasClass(dc.showClass)) {
                toggleLine(dc.currentOpen, false);
            }
            dc.currentOpen = o;
        }

        //dc.cancelClick(e);
    },
    getTarget: function(e) {
        var target = window.event ? window.event.srcElement : e ? e.target : null;
        if (!target) {
            return false;
        }
        while (!target.tohide && target.nodeName.toLowerCase() != 'body') {
            target = target.parentNode;
        }
        // if (target.nodeName.toLowerCase() != 'a'){target = target.parentNode;} Safari fix not needed here
        return target;
    },
    cssjs: function(a, o, c1, c2) {
        switch (a) {
        case 'swap':
            o.className = !dc.cssjs('check', o, c1) ? o.className.replace(c2, c1) : o.className.replace(c1, c2);
            break;
        case 'add':
            if (!dc.cssjs('check', o, c1)) {
                o.className += o.className ? ' ' + c1 : c1;
            }
            break;
        case 'remove':
            var rep = o.className.match(' ' + c1) ? ' ' + c1 : c1;
            o.className = o.className.replace(rep, '');
            break;
        case 'check':
            return new RegExp("(^|\\s)" + c1 + "(\\s|$)").test(o.className);
        }
    }
};
$(function() {
    dc.init();
});

