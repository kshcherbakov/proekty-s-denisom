function Scheme(container,stations) {
  this.container = container;
  this.stations = stations;
  this.zones = [];
  var maxY = 0;


  for(i in this.stations) {
    this.zones[this.stations[i].zone] = this.stations[i].id;
  }

  for(i in this.zones) {
    this.container.append("<div class='zone' data-zone='"+i+"'></div>");
  }

  // ��������� �������
  for(i in this.stations) {
    $(".zone[data-zone="+this.stations[i].zone+"]",this.container).append("<div class='station-item "+(this.stations[i].selected ? "station-selected" : "")+" "+(this.stations[i].titlePosition=="left" ? "station-title-left" : "")+"' data-id='"+this.stations[i].id+"' data-num-id='"+i+"' style='left:"+this.stations[i].x+"px; top:"+this.stations[i].y+"px'><span>"+this.stations[i].name+"</span></div>");
    if(this.stations[i].y > maxY) maxY = this.stations[i].y;
  }
  this.container.height(maxY+50);
  
}

// �������� ������ �������� �������
Scheme.prototype.getNeighbors = function(k){
  var neighbors = [];
  for (m in this.stations[k].links) {
    if(!isNaN(parseInt(this.stations[k].links[m]))) {
      var num = this.getNumByID(this.stations[k].links[m]);
      neighbors[neighbors.length] = { 
        zone : this.stations[ num ].zone,
        id : this.stations[num].id,
        num : num,
        name : this.stations[num].name,
        overflow : false,
        x : this.stations[num].x,
        y : this.stations[num].y,
        selected : this.stations[num].selected
      };
    } else {
      neighbors[neighbors.length] = { 
        num : this.stations[k].links[m],
        overflow : true
      };
    }
  }
  return neighbors;
}

// �������� ����� ������� �� �� id
Scheme.prototype.getNumByID = function(id) {
  for(l in this.stations) {
    if(this.stations[l].id == id) return l;
  }
  return 0;
}

// �������� ���� ������� �� �����
Scheme.prototype.setSelected = function(id) {
  this.container.find(".station-item[data-id="+id+"]").addClass("station-selected");
  this.stations[this.getNumByID(id)].selected = true;
}
// ������ ��������� ����� ������� �� �����
Scheme.prototype.setUnselected = function(id) {
  this.container.find(".station-item[data-id="+id+"]").removeClass("station-selected");
  this.stations[this.getNumByID(id)].selected = false;
}

// ����� ��������� ���� ������� �� �����
Scheme.prototype.selectRemove = function() {
  var remove = [];
  container.find(".station-item.station-selected").each(function(){
    this.removeClass("station-selected");
    remove[remove.length] = this.attr("data-id");
  });
  for(i=0;i<remove.length;i++) {
    this.stations[this.getNumByID(id)].selected = false;
  }
}


function canvasObj(container) {  
  this.offsetX = 6.5;
  this.offsetY = 16.5;
  this.routeRadius = 20;
  this.canvas = ""
  this.built = "";
  this.lineWidth = 5;
}

canvasObj.prototype.build = function(scheme){
  scheme.container.append("<canvas id='scheme_routes' width='"+scheme.container.width()+"' height='"+scheme.container.outerHeight()+"'></canvas>");
  this.canvas = document.getElementById("scheme_routes").getContext("2d");
}


canvasObj.prototype.renderRoute = function(scheme) {
  var inst = this;

  $(".station-item:not(.station-empty)").each(function(){
      var i = $(this).attr("data-num-id");
      var thisX = scheme.stations[i].x;
      var thisY = scheme.stations[i].y;
      var neighbors = scheme.getNeighbors(i);
      for(j in neighbors) {
        if(!neighbors[j].overflow) {
          if(inst.built.indexOf(thisX+","+thisY+"-"+neighbors[j].x+","+neighbors[j].y)==-1) {
            inst.renderCut(scheme.stations[i], scheme.stations[scheme.getNumByID(neighbors[j].id)]);
          }
        }
      }
  });
}

canvasObj.prototype.renderCut = function(point1, point2, deleteCut) {
  var x1 = point1.x; 
  var y1 = point1.y; 
  var x2 = point2.x;
  var y2 = point2.y;
  this.canvas.beginPath();
  this.canvas.moveTo(x1+this.offsetX, y1+this.offsetY);
  if(x2 == x1 || y2 == y1) {
    this.canvas.lineTo(x2+this.offsetX,y2+this.offsetY);
  } else {
    if(x2 > x1 && point1.titlePosition != "left") {
      this.canvas.arcTo(x1+this.offsetX,y2+this.offsetY,x2+this.offsetX,y2+this.offsetY, this.routeRadius);
      this.canvas.lineTo(x2+this.offsetX,y2+this.offsetY);
    } else {
      this.canvas.arcTo(x2+this.offsetX,y1+this.offsetY,x2+this.offsetX,y2+this.offsetY, this.routeRadius);
      this.canvas.lineTo(x2+this.offsetX,y2+this.offsetY);
    }
  }
  this.built += x2+","+y2+"-"+x1+","+y1+";";

  this.canvas.lineWidth = this.lineWidth;
  if(point1.selected && point2.selected)
    this.canvas.strokeStyle = "#ff6503";
  else 
    this.canvas.strokeStyle = "#00343f";
  this.canvas.stroke();
}

canvasObj.prototype.clearRoute = function(scheme){
  this.canvas.clearRect(0, 0, scheme.container.width(), scheme.container.width());
}


$(function(){
  var container = $(".scheme-electro");
  var scheme = new Scheme(container,stations);
  var canvas = new canvasObj(scheme, container); 

  canvas.build(scheme);
  canvas.renderRoute(scheme);

  $(".station-item", container).click(function(){
    switch(container.find(".station-selected").length) {
      case(0) : {
        scheme.setSelected($(this).attr("data-id"));
        break;
      }
      case(1) : {
        $.ajax({
          url : "getRoute.json",
          dataType: "json",
          type : "post",
          data : {
            start : container.find(".station-selected").attr("data-id"),
            finish : $(this).attr("data-id")
          },
          success: function(data){
            for(i=0; i< data.stations.length;i++) {
              scheme.setSelected(data.stations[i]);
            }
            canvas.clearRoute(scheme);
            canvas.renderRoute(scheme);
          }    
        });
        break;
      }
      default : {
        for(i=0;i<scheme.stations.length;i++) {
          if(scheme.stations[i].selected) {
            scheme.setUnselected(scheme.stations[i].id);
          }
        }
        scheme.setSelected($(this).attr("data-id"));
        canvas.clearRoute(scheme);
        canvas.renderRoute(scheme);
      }
    }
  }); 

})
