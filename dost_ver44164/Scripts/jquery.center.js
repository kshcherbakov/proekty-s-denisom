﻿jQuery.fn.center = function() {
    this.makeAbsolute(true);
    //this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 4 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
};

$.fn.makeAbsolute = function(rebase) {
    return this.each(function() {
        var el = $(this);
        var pos = el.position();
        el.css({ position: "absolute",
            marginLeft: 0, marginTop: 0,
            top: pos.top, left: pos.left
        });
        if (rebase)
            el.remove().appendTo("body");
    });
}