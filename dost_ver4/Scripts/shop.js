$(document).ready(function(){
  
  $(".shop-article-gallery-item-thumbnail").click(function(){
    $(".shop-article-gallery-item").removeClass("active");
    $(this).parent(".shop-article-gallery-item").addClass("active");
    return false;
  })
  
  $(".shop-article-gallery-item-large").each(function(){
    $(this).click(function(){
      $.colorbox({
        href: $(this).attr("colorbox-href"),
        photo: true
      })
    })
  })
  
});