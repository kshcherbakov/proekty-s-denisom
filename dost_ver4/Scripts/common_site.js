if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace( /^\s+|\s+$/g , '');
    };
}

$.fn.scrollView = function() {
    return this.each(function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 84
        }, 1000);
    });
};

var tooltip = false;

$(function () {

    // Обработка сортировки списка поездов Route_Details
    $("div.table-head a.sortBlock-a").live("click", function() {
        var form = $("#setTrainForm");

        form.children("input[name$=Sort]").val($(this).attr("sort"));
        form.children("input[name$=Direction]").val($(this).attr("dir"));

        $("a.sortBlock-a").each(function() {
            $(this).removeClass("sortBlock-up-selected");
            $(this).removeClass("sortBlock-down-selected");
        });

        if ($(this).attr("dir") == "Up") {
            $(this).toggleClass("sortBlock-up-selected");
        } else {
            $(this).toggleClass("sortBlock-down-selected");
        }

        $.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: form.attr("action"),
            data: form.serialize(),
            success: function(data) {
                $.each(data.nums, function (index) {
                    form.append($("div[train=" + data.nums[index] + "]"));
                });
            }
        });
        return false;
    });

    $("div.thead span.dashed").live("click", function () {
        var form = $("#setTrainForm");

        $(form).children("input[name$=Sort]").val($(this).attr("sort"));
        $(form).children("input[name$=Direction]").val($(this).attr("dir"));

        $(form).children("div.indexTable").removeClass().addClass("indexTable").addClass("sort-" + $(this).attr("sort")).addClass("sort-" + $(this).attr("dir"));

        if ($(this).attr("dir") == "up") {
            $(this).attr("dir", "down");
        } else {
            $(this).attr("dir", "up");
        }

        $.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data) {
                $.each(data.nums, function (index) {
                    $("#trainsSec").append($("section[train=" + data.nums[index] + "]"));
                });
            }
        });
        return false;
    });

    // Выбор конкретного поезда
    $("button.submitButton").live("click", function () {
        $(this).parents("td.table-row-item").children("input[name$=IsSelected]").val(true);
    });

    // Обработка переключения табов с телефонами в шапке
    $("#header-contacts").tabs({
        selected: window.CurrentContactsTab,
        cookie: { expires: 1 },
        select: function (event, ui) {
            $.ajax({
                type: "POST",
                url: "/Contacts/AjaxSetVisitorCity",
                data: { city: ui.index },
                cache: false,
                success: function (response) {
                    $("#footerPhone").html(response);
                }
            });
            var url = "";
            if ($("div.navBottom-wrapper #rulesBlock").length)
                url += "/Layout/AjaxBottomMenuRules";
            else
                url += "/Home/AjaxRules";

            $.ajax({
                type: "POST",
                url: url,
                data: { city: ui.index },
                cache: false,
                success: function (response) {
                    $("#rulesBlock").html(response);
                }
            });
        }
    });

    $("div.popularList").columnize({ columns: 2, lastNeverTallest: true });

    $("#popular-opening a, .callBack-opening a, #callback-opening a, #fwdTicket-opening").click(function () {
        openPoupUp(this);
        return false;
    });

    $("#popular-opening a, .callBack-opening a").live("click", function () {
        openPoupUp(this);
        return false;
    });

    $("#popular-close span, #popular-title-close, #callback-close span, #callback-title-close, .callback-title-close, .callback-close, #fwdTicket-close").click(function () {
        $(this).parents("div.popupBlock").removeClass("popupBlock-open");
    });

    $("#popular-close span, #popular-title-close, #callback-close span, #callback-title-close, .callback-title-close, .callback-close, #fwdTicket-close").live("click", function () {
        $(this).parents("div.popupBlock").removeClass("popupBlock-open");
    });

    $("div.popupBlock").click(function () {
        return false;
    });

    /* закрывает все всплывающие окна */
    $(".content, .header, .top-nav, .navBottom, .footer").click(function () {
        if (tooltip) {
            closePoupUps();
            tooltip = false;
        }
        //    $(".popupBlock").removeClass("popupBlock-open");
    });


    $("span.yearSheduleTable-is").click(function () {
        $("span.yearSheduleTable-is").removeClass("yearSheduleTable-is-select");
        $(this).addClass("yearSheduleTable-is-select");
    });
    $(".trainSel a").click(function () {
        $(this).parent().parent().toggleClass("trainSel-open");
    });
    $(".trainSel-close span").click(function () {
        $(this).parent().parent().parent().removeClass("trainSel-open");
    });

    /*форма вызова звонка оператора*/
    $('#OperatorForm').ajaxForm({
        url: "/Request/OperatorForm",
        success: function () {
            var message = "Ваш запрос на вызов оператора принят.";
            showMessage(message, 300, 70);

            //очищаем форму
            $('#Name').val('');
            $('#Phone').val('');
            $('#TimeFrom').val('');
            $('#TimeTo').val('');
            $('#Comment').val('');
        }
    });

    /*форма предложения руководителю*/
    $('#dirCallForm').submit(function () {
        $('#operatorSubmitButton').hide();
        $('#operatorImgLoader').show();
    });

    $('#dirCallForm').ajaxForm({
        url: "/Request/DirectorCallForm",
        beforeSubmit: operatorValidator,
        success: function () {
            var message = "Ваши предложения руководителю компании приняты.";
            showMessage(message, 300, 70);

            $('#operatorSubmitButton').show();
            $('#operatorImgLoader').hide();

            //очищаем форму
            $('#name').val('');
            $('#phone').val('');
            $('#email').val('');
            $('#comment').val('');
        }
    });

    $('#TicketSearchForm').ajaxForm({
        url: "/Request/TicketSearchForm",
        success: function () {
            var message = "Ваш запрос принят.";
            showMessage(message, 300, 70);
            
            //очищаем форму
            $('#From').val('');
            $('#To').val('');
            $('#Fio').val('');
            $('#Phone').val('');
            $('#Email').val('');
            $('#Comment').val('');
        }
    });
    
    function operatorValidator() {
        var result = true;
        $('.input-validation-error').removeClass('input-validation-error');
        if ($('#name').val() == '' && result) {
            $('#name').addClass('input-validation-error');
            $('#name').focus();
            result = false;
        }

        if ($('#phone').val() == '' && result) {
            $('#phone').addClass('input-validation-error');
            $('#phone').focus();
            result = false;
        }

        if ($('#email').val() == '' && result) {
            $('#email').addClass('input-validation-error');
            $('#email').focus();
            result = false;
        }

        $('#operatorSubmitButton').show();
        $('#operatorImgLoader').hide();
        return result;
    }

    $(".shedule-links").live("click", function () {
        $.ajax({
            url: "/shedule/trainShedule",
            data: { fromId: $(this).attr('from'), toId: $(this).attr('to'), shedule: $(this).attr('days') },
            type: "GET",
            cache: false,
            success: function (html) {
                // показываем всплываеющее окно
                $.colorbox({
                    inline: true,
                    href: '#calendarLightbox',
                    close: 'закрыть',
                    overlayClose: false,
                    className: true,
                    onOpen: function () {
                        $('#calendarLightbox').html(html);
                        $('#cboxClose').removeClass('hidden');
                    }
                });
            }
        });
    });
});

//дни в которые поезд не ходит
var disabledDays;

//дни в которые открыта продажа
var enabledDays;

function isDateInArray(date, daysArray) {
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    for (var i = 0; i < daysArray.length; i++) {
        if ($.inArray((m + 1) + '-' + d + '-' + y, daysArray) != -1 || new Date() > date) {
            return [true];
        }
    }
    return [false];
}

function noWeekendsOrHolidays(date) {
    return isDateInArray(date, disabledDays);
}

function isDayActive(date) {
    return isDateInArray(date, enabledDays);
}

$(function () {

    //работа с календарем datepicker

    $.datepicker.setDefaults($.datepicker.regional["ru"]);
    $.datepicker.setDefaults({
        nextText: '',
        prevText: '',
        minDate: 0,
        numberOfMonths: window.NumberOfMonths,
        showButtonPanel: true,
        beforeShowDay: isDayActive,
        closeText: 'Закрыть'
    });
    $.datepicker.setDefaults({
        beforeShow: function (input) {
            $(input).parent("div.input-field-date").addClass("input-field-date-active");
            $(input).parent("div.sheduleSearch-inputBlock").addClass("input-field-date-active");
        },
        onClose: function () {
            $("div.input-field-date").removeClass("input-field-date-active");
            $("div.sheduleSearch-inputBlock").removeClass("input-field-date-active");
        }
    });

    $("#DateChange").datepicker({
        onSelect: function(dateText) {
            $("#Date").val(dateText);
            $("#DateChangeForm").submit();
        }
    });

    $("#Date, #DateBack, #SearchDate, .sheduleSearch-date-input").datepicker();

    $(".input-field-date span, .dates-choose, .routeInfo-date-choose, .sheduleSearh-date-span").click(function () {
        $(this).prev("input").datepicker("show");
    });

    $(".routeInfo-date span").click(function () {
        $(this).parent().children("input").datepicker("show");
    });

    $('#chb-direction').click(function () {
        if ($('#chb-direction').html() == 'Только туда!') {
            $('#DateBack').val('');
            $('#chb-direction').html('Дата обратно');
            $('#DateBack').parent().parent().hide();
        } else {
            $('#chb-direction').html('Только туда!');
            $('#DateBack').parent().parent().show();
        }
    });
    $("body").append("<div id='ToolTip'></div>");

    //
    // Discounts kiosk
    //
    $(".discounts-open, .discounts-close", "#discounts").click(function () {
        $("#discounts").toggleClass("opened");
    });

    //
    // Filter kiosk
    //
    $(".filter-open, .filter-close, h3", "#filter").live("click", function () {
        $("#filter").toggleClass("opened");
    });

    //
    // Info kiosk
    //
    $(".info > span").live("click", function () {
        var infoCaller = $(this);
        closeInfoKiosks(function () {
            showInfoKiosk(infoCaller);
        });

    });

});

//////////////////////////////
// Info kiosk functions
//

    function showInfoKiosk(infoCaller) {
      var coord = infoCaller.offset();
      var infoKiosk = $("#info_kiosk");
      var contentType = infoCaller.parent().attr("content");
      infoKiosk.removeAttr("style");
      infoKiosk.html("");
      infoKiosk.attr("content",contentType);
      $(".info_content:has(."+contentType+"_wrapper)").clone().appendTo(infoKiosk);
      infoKiosk.children("div").prepend("<div class='border-header'></div>");
      infoKiosk.css("left",coord.left-7);
      infoKiosk.css("top",coord.top);
      infoKiosk.css("margin-top",(parseInt("-"+(infoKiosk.outerHeight()-15))+parseInt(infoKiosk.css("margin-top")))+"px");
      infoCaller.addClass("hidden");
      infoKiosk.fadeIn('fast',function(){
        $("body").bind('click', function(){
          closeInfoKiosks();
        });
      });
    }
    function closeInfoKiosks(callback) {
      if($("#info_kiosk:visible").length) {
        $(".info > span").removeClass("hidden");
        $("#info_kiosk:visible").fadeOut(function(){
          if(callback) unbindInfoKiosk(callback);
        });
      } else {
        if(callback) unbindInfoKiosk(callback);
      }
    }
    function unbindInfoKiosk(callback) {
      $("#info_kiosk").removeClass();
      $("body").unbind("click");
      if(callback) callback();
    }
    
//
// End of Info kiosks functions
/////////////////////////////////

function showLoader(message) {
    if (message != '') $('div#loader div#loader-message').html(message);
    $('div#loader').show();
}

function hideLoader() {
    $('div#loader').hide();
}

// Показывает окно с загрузкой
function showLoadingWindow(fullSize) {
    // показываем всплываеющее окно
    $.colorbox({
        inline: true,
        href: '#loading',
        close: 'закрыть',
        overlayClose: false,
        className: true,
        onOpen: function () {
            if (!fullSize) {
                $('#loadingBicycle').hide();
                $('#loadingNote').hide();
            }
            $('#cboxClose').addClass('hidden');
        }
    });
}

// Скрывает окно с загрузкой
function hideLoadingWindow() {
    $.colorbox.close();
}

function setStation(id, value) {
    $('#' + id).val(value);
    $('#' + id + 'List').html('');
}

function openPoupUp(obj) {
    $(".popupBlock").removeClass("popupBlock-open");
    if (obj != null) {
        $(obj).parents("div.popupBlock").toggleClass("popupBlock-open");
        closeInfoKiosks();
    }
}

function closePoupUps() {
    $('#FromList').html('');
    $('#ToList').html('');
    $('#PopularRoutes').hide();
    $("#ToolTip").hide();
    tooltip = false;
}

function setStations(obj, from, to) {
    $('#From').val(from);
    $('#To').val(to);
    $(obj).parents("div.popupBlock").removeClass("popupBlock-open");
    //$('#PopularRoutes').hide();
    //$("#ToolTip").hide();
}

function showMeteo (loaction, date, placeholder) {
    jQuery('#' + placeholder).html("<img src='/Content/images/lightbox-ico-loading.gif'>");
    $.ajax({
        type: "GET",
        url: "/wheather/index",
        data: { cityName: loaction, date: date },
        success: function (response) {
            jQuery('#' + placeholder).html(response);
        }
    });
}



/*TOOLTIPS*/
var mouseX = 0;
var mouseY = 0;

$(document).mousemove(function (e) {
    mouseX = e.pageX;
    mouseY = e.pageY;
});

function showToolTip(id, width, height) {
    closePoupUps();
    var x = mouseX;
    var y = mouseY;
    $("#ToolTip").show();
    $("#ToolTip").html("<img src='/Content/images/small-loader.gif'>");
    $.ajax({
        type: "GET",
        url: "/hint/" + id,
        success: function (response) {
            $("#ToolTip").html(response);
            $("#ToolTip").css({ left: x + 20, top: y - $("#ToolTip .inner").height() / 2 });
            if (width > 0) {
                $("#ToolTip .inner").css({ width: width });
                $("#ToolTip div.popupBlock-open div.callback-form").css({ width: width + 30 });
            }
            if (height > 0) {
                $("#ToolTip .inner").css({ height: height });
            }
            tooltip = true;
        }
    });
}

function showTextToolTip(text, width, height) {
    closePoupUps();
    $("#ToolTip").css({ left: mouseX + 20, top: mouseY - $("#ToolTip .inner").height() / 2 });
    $("#ToolTip").show();
    $("#ToolTip").html("<img src='/Content/images/small-loader.gif'><br/>Загрузка погоды");
    $.ajax({
        type: "GET",
        url: "/hint/0",
        success: function (response) {
            $("#ToolTip").html(response);
            $("#ToolTip .inner").html(text);
            if (width > 0) {
                $("#ToolTip .inner").css({ width: width });
                $("#ToolTip div.popupBlock-open div.callback-form").css({ width: width + 30 });
            }
            if (height > 0) {
                $("#ToolTip .inner").css({ height: height });
            }
            tooltip = true;
        }
    });
}


var toolTipIndex = 1;
$(document).ready(function () {

    /* Работа с выпадающими списками */

    $(".autocomplete").keyup(function (event) {
        var key = event.which;
        var plh = $(this).attr("ajaxlist");
        //вниз
        if (key == 40 && top.tooltip) {
            toolTipIndex++;
            if ($('.station-' + toolTipIndex).html() == null) toolTipIndex = 2;
            $(this).val($('.station-' + toolTipIndex).html().trim());

            $('.station-item, .station-item-last').each(function () {
                $(this).removeClass('station-item-sel');
            });
            $('.station-' + toolTipIndex).toggleClass("station-item-sel");
            return;
        }
        //вверх    
        if (key == 38 && top.tooltip) {
            toolTipIndex -= 1;
            if ($('.station-' + toolTipIndex).html() == null) toolTipIndex = $('.stations-list div').length;
            $(this).val($('.station-' + toolTipIndex).html().trim());

            $('.station-item, .station-item-last').each(function () {
                $(this).removeClass('station-item-sel');
            });
            $('.station-' + toolTipIndex).toggleClass("station-item-sel");
            return;
        }
        var station = $(this).val();
        if (station.length < 3) return;
        closePoupUps();
        $.ajax({
            type: "POST",
            url: $(this).attr("ajaxurl"),
            data: { name: station, id: $(this).attr("id") },
            success: function (response) {
                $('#' + plh).html(response);
                top.tooltip = true;
                toolTipIndex = 1;
            }
        });
    });

    // Show float header if body has an approrpiate class
    if ($("body.floatHeader").length) {
        floatHeeder();
        $(document).scroll(function () {
            floatHeeder();
        });
    }

    /* Currency choose */
    $("div.price button", ".thead").live("click", function () {
        $(".price-choose").fadeIn(function () {
            $("body").bind('click', function () {
                $(".price-choose").fadeOut(function () {
                    $("body").unbind('click');
                });
            });
        });
        return false;
    });
    $(".price-choose span", ".thead").live("click", function () {
        if (!$(this).hasClass("price-choose-selected")) {
            $(".price-choose span").removeClass("price-choose-selected");
            $(this).addClass("price-choose-selected");
            $("div.price button", ".thead").text($(this).text());

            $("#Currency").val($(this).attr("curr"));
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: "/Step1/ReCalcPrices",
                data: $("#setTrainForm").serialize(),
                success: function (data) {
                    $.each(data.cats, function (index) {
                        var button = $("section[train=" + data.cats[index].Train + "]").find("button[cat=" + data.cats[index].Cat + "]");
                        button.html(data.cats[index].Price);
                        button.attr("title", "Тариф: " + data.cats[index].Tariff + " сборы: " + data.cats[index].Service);
                    });
                }
            });

            $(".price-choose").fadeOut();
        }
        ;
    })
    $("body").keyup(function (e) {
        if (e.which === 27) {
            if ($(".price-choose:not(:hidden)").length) $(".price-choose").fadeOut();
        } // 27 is the keycode for the Escape key
    });
});

/* MessageWindow */
function showMessage(text, width, height) {

    // показываем всплываеющее окно
    $.colorbox({
        inline: true,
        href: '#callbackComplite',
        close: 'закрыть',
        overlayClose: false,
        className: true,
        onOpen: function () {
            $('#callbackComplite').children("p").html(text);
            $('#cboxClose').removeClass('hidden');
        }
    });

    return false;
}

//
// function for setting of float header
//
function floatHeeder() {
      if($(this).scrollTop() > 70) $("body").addClass("scrolled");
      else  $("body").removeClass("scrolled");
}

/* Запрос на установку / снятие ЭР */
function erCheck(id, obj) {
    if ($(obj).val() == 'CheckIn') {

        $(obj).attr("disabled", "disabled");
        $(obj).parents("div.profileOrders-block-er").children("#ajaxLoader").show();

        $.ajax({
            url: "/Account/RemoteCheckIn/",
            type: "POST",
            cache: false,
            dataType: "json",
            data: { transId: id, checkIn: true },
            success: function(data) {
                if (data != '0') {
                    alert(data);
                } else {
                    $(obj).attr('src', '/Content/Images/eregout2.png');
                    $(obj).val('CheckOut');
                }
                $(obj).removeAttr("disabled");
                $(obj).parents("div.profileOrders-block-er").children("#ajaxLoader").hide();
            }
        });
    } else if ($(obj).val() == 'CheckOut') {

        $(obj).attr("disabled", "disabled");
        $(obj).parents("div.profileOrders-block-er").children("#ajaxLoader").show();

        $.ajax({
            url: "/Account/RemoteCheckIn/",
            type: "POST",
            cache: false,
            dataType: "json",
            data: { transId: id, checkIn: false },
            success: function(data) {
                if (data != '0') {
                    alert(data);
                } else {
                    $(obj).attr('src', '/Content/Images/eregin2.png');
                    $(obj).val('CheckIn');
                }
                $(obj).removeAttr("disabled");
                $(obj).parents("div.profileOrders-block-er").children("#ajaxLoader").hide();
            }
        });
    }
    return false;
}