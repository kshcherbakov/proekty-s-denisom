$(function(){

  // Формируем массив типов вагонов и вычисляем 
  // общее количество свободных мест для каждого типа вагона.
  // Выводим данные по шаблону.
  // Из списка вагонов типы объединяем
  var wagonTypes = new Array();
  for(i=0;i<wagons.length;i++) {
    if((existsIndex=searchExistType(wagons[i].name,"name",wagonTypes)) && (existsIndex=searchExistType(wagons[i].num,"num",wagonTypes))) {
      existsIndex = existsIndex.split("|")[1];
      wagonTypes[existsIndex]['free']+=wagons[i]['free'];
      if(wagonTypes[existsIndex]['free_top']) {
        wagonTypes[existsIndex]['free_top']+=wagons[i]['free_top'];
        wagonTypes[existsIndex]['free_bottom']=wagonTypes[existsIndex]['free']-wagonTypes[existsIndex]['free_top'];
      }
    } else {
      newCount = wagonTypes.length;
      wagonTypes[newCount] = {};
      for(key in wagons[i]) {
        wagonTypes[newCount][key] = wagons[i][key];
        wagonTypes[newCount]["i"] = i;
      }
      if(wagonTypes[newCount]['free_top'])  wagonTypes[newCount]['free_bottom']=wagonTypes[newCount]['free']-wagonTypes[newCount]['free_top'];
      else wagonTypes[newCount]['free_bottom'] = (wagonTypes[newCount]['free']);
    }
  }
  $('#wagonType').tmpl(wagonTypes).appendTo('#wagonTypes');

  // чекбоксы с фильтром мест
  $(".wagon-tabs").each(function(){
    if($(this).hasClass("placTabs") || $(this).hasClass("svTabs") || $(this).hasClass("coupeTabs") || $(this).hasClass("jointTabs") || $(this).hasClass("jointTabs")) {
      var conditions = $("body > .wagon-conditions").clone();
	  // скрываем лишние фильтры
	  $(conditions).find("ul li label:not(["+$(this).attr("type")+"]").each(function( index ) {
		$(this).hide();
	  });
	  
	  conditions.prependTo($(this).parent(".wagon-scheme"))	  
	  
      $("input[type=checkbox]",$(this).siblings(".wagon-conditions")).click(function(){
        applyFilter($(this));
      });
    }
  })

  // Раскрытие типов вагонов: вешаем обработчик кликов на созданный код
  $("td.wagonNum span, td.title span, td.choose button.open, td.choose button.close", "#placesTable").click(function(){
    var wagonType = $(this).parents("table");
    placesChoose_toggle(wagonType);
  })
  
  if(active_Wagon) {
    var activeH4 = $("td.title span > span:contains('"+active_Wagon+"')","#placesTable");
    activeH4.click();
    scrollToActive(activeH4);
  }

  // скрываем легенду по улолчанию
  $(".wagon-legend").addClass("wagon-legend-hidden");

})


//
// Прокручивает страницу к активному типу вагона
//
function scrollToActive(title) {
  var scrollToH4 = title.offset().top - 100;
  $('html, body').stop().animate({scrollLeft: 0, scrollTop: scrollToH4}, 1000);
}

// Функция ищет заданное значение в объекте.
// В случае успеха возвращает строку с ключами свойств найденного значения
function searchExistType(query,property,object,prop) {
  if(typeof prop == 'undefined') prop ='';
  for(key in object) {
    if(typeof object[key] == 'object') {
      var prop_new = searchExistType(query,property,object[key],prop+"|"+key);
      if(prop_new) return prop_new;
    } else if(key==property) {
      if(object[key] == query) {
        return prop + "|" + key;
      }
    }
  }
  return false;
}


// Функция Открывает и закрывает тип вагонов для выбора места
function placesChoose_toggle(wagonType, userAgree) {// alert(wagonType.html());
  if($(".wagon-section > div.checked").length) {
    if(typeof userAgree == 'undefined') {
      var infoCaller = wagonType.find(".title");
      closeInfoKiosks(function() {
        showInfoKiosk(infoCaller,"attention_alreadySelected");
      });
      $(".attention_accept").live("click",function(){ 
        placesChoose_toggle(wagonType,1);
        $(".attention_accept").die("click");
        return false;
      });
      $(".attention_decline").live("click",function(){
        placesChoose_toggle(wagonType,0);
        $(".attention_decline").die("click");
        return false;
      });
      return false;
    } else {
      if(userAgree == 0) return false;
    }
  }
  $(".wagon-section > div.checked").each(function(){
    selectPlaces($(this));
  });
  if(wagonType.hasClass("opened")) {
    wagonType.removeClass("opened");
  } else {
    $("table","#wagonTypes").removeClass("opened");
    wagonType.addClass("opened");
    showTabs(wagonType); //alert(wagonType.find("h4").find("span").text());
    scrollToActive(wagonType.find("h4"));
  }
}


// Функция выводит табы со списком вагонов в контейнер заданного типа вагонов
function showTabs(wagonType) {
  // Если вагоны уже отрисованы для этого типа...
  if(wagonType.find(".wagon-tabs li").length) return false;
  var wagonType_name = $(".wagon-name",wagonType).text();
  var selectedWagon = new Array();
  for(i=0;i<wagons.length;i++) {
    if(wagons[i].num + "-" + wagons[i].name == wagonType_name) {
      newCount = selectedWagon.length;
      selectedWagon[newCount] = {};
      for(key in wagons[i]) {
        selectedWagon[newCount][key] = wagons[i][key];
      }
    }
  }
  $(wagonListUL).tmpl(selectedWagon).appendTo($('.wagon-tabs > ul',wagonType));
  $(wagonListDiv).tmpl(selectedWagon).appendTo($('.wagon-tabs',wagonType));
  wagonTabs($('.wagon-tabs',wagonType));
}

// Функция отрисовывает /*доступные*/ места в вагоне
function showPlaces(wagon) {
  // Если места уже отрисованы для этого вагона...
  if(wagon.find(".wagon-section").length) {
    // применение уже выделенных фильтров
    wagon.parent(".placTabs").next(".wagon-conditions").find("input[type=checkbox]").each(function(){
      applyFilter($(this));
    });
    return false;
  }
  var wagonType = wagon.attr("type");
  var wagonNum = wagon.attr("num");
  
  // отрисовка секций для стандартных купейных, плацкартных и СВ вагонов и стрижа
  if(wagonType=="sv" || wagonType=="coupe" || wagonType=="plac" || wagonType=="coupe_2f" || wagonType=="sv_2f") {
    
    if (wagonType =="coupe_2f" || wagonType=="sv_2f") var sectionQuantity = 16;
    else var sectionQuantity = 9;
    
    for(i=0;i<sectionQuantity;i++) {
      $('#wagonSection').tmpl({num:i}).appendTo(wagon);
    }
    // отрисовка мест для вагона
    if(typeof(places[wagonNum-1]) != "undefined") {
      for(i=0;i<places[wagonNum-1].length;i++) {
        $('#wagonSectionPlaces').tmpl(places[wagonNum-1][i]).appendTo($(".wagon-section:eq("+(places[wagonNum-1][i].coupe_num-1)+")",wagon));
        var lastPlace = $("div[place]:last",wagon);
        var gender = lastPlace.attr("gender");
        if(gender) lastPlace.parent("div.wagon-section").attr("gender",gender);
      }
    }
    // применение уже выделенных фильтров
    wagon.parent(".placTabs").next(".wagon-conditions").find("input[type=checkbox]").each(function(){
      applyFilter($(this));
    });
  }
  
  // Отрисовка секций для сапсана и стрижа
  if(wagonType=="saps1_" || wagonType=="saps2_" || wagonType=="saps3_" || wagonType=="saps4_" || wagonType=="saps5_" || wagonType=="saps6_" || wagonType=="saps7_" || wagonType=="saps8_" || wagonType=="saps9_" || wagonType=="saps10_" || wagonType=="strizh_lux"  || wagonType=="strizh_middle"  || wagonType=="strizh_econom") {
    $('#wagonSection').tmpl({num:0}).appendTo(wagon);
    // отрисовка мест для вагона
    if(typeof(places[wagonNum-1]) != "undefined") {
      for(i=0;i<places[wagonNum-1].length;i++) {
        $('#wagonSectionPlaces').tmpl(places[wagonNum-1][i]).appendTo($(".wagon-section",wagon));
      }
    }
  }
  
  // Отрисовка схемы по-умолчанию
  if(wagonType=="default" || wagonType=="joint" || wagonType=="sit") {
    $('#wagonSectionDefault').tmpl({num:0}).appendTo(wagon);
    if(typeof(places[wagonNum-1]) != "undefined") {
      for(i=0;i<places[wagonNum-1].length;i++) {
        $('#wagonSectionPlaces').tmpl(places[wagonNum-1][i]).appendTo($(".wagon-section",wagon));
      }
	  $('input[name="rangePlaceFrom"]',wagon).val(places[wagonNum-1][0].place_num);
	  $('input[name="rangePlaceTo"]',wagon).val(places[wagonNum-1][places[wagonNum-1].length-1].place_num);
	  
	  $(".sortBlock-up",wagon).click(function(){
		var input = $(this).parents("div.input-field-text").children("input[type='text']");
		var max = Math.min(places[wagonNum-1].length, 4);		
		var i = parseInt(input.val()) + 1;
		  
		var selCount = 0;
		$(this).parents("div.place-quantity-section").find("input").each(function(){
		  selCount += parseInt($(this).val());
		});
		
		if (input.attr('name') == 'baby') {
		  adults = 0
		  $(this).parents("div.place-quantity-section").find("input[name='adult']").each(function(){
		    adults += parseInt($(this).val());
		  })
		  if (selCount < max && i <= adults) {
		    input.val(i);
		  }
		} else {
		  if (selCount < max) {
		    input.val(i);
		  }
		}
		
		// перещет суммы заказа
		var placeCount = 0
		$(this).parents("div.place-quantity-section").find("input[name='adult'], input[name='child']").each(function(){
		  placeCount += parseInt($(this).val());
		})
		         
        var totalPrice = 0;
		for (var i = 0; i < placeCount; i++) {
	      totalPrice += parseInt(places[wagonNum-1][i].price);
	    }
		$(this).parents("div.wagon-wrapper").find(".ticketData-total .ticketData-totalPrice").text(totalPrice+" "+price_currency);
		var button = $(this).parents("div.wagon-wrapper").find(".ticketData-lineOptions button");
		button.css("color", "#00216c");
		button.css("cursor", "pointer");
	  });
	  
	  $(".sortBlock-down",wagon).click(function(){
		var input = $(this).parents("div.input-field-text").children("input[type='text']");
		var i = parseInt(input.val()) - 1;
		var max = places[wagonNum-1].length;
		if (i <= max && i >= 0) {
		  input.val(i);
		  // перещет суммы заказа
		  var placeCount = 0
		  $(this).parents("div.place-quantity-section").find("input[name='adult'], input[name='child']").each(function(){
		    placeCount += parseInt($(this).val());
		  })
		
          var totalPrice = 0;
		  for (var i = 0; i < placeCount; i++) {
	        totalPrice += parseInt(places[wagonNum-1][i].price);
	      }
		  $(this).parents("div.wagon-wrapper").find(".ticketData-total .ticketData-totalPrice").text(totalPrice+" "+price_currency);
		}
	  });
    }
  }
  
  // обработчик клика по месту
  $("div.wagon-section > div[place]",wagon).click(function(){
    selectPlaces($(this));
  })
  
  // Заранее выбранные номера места, код срабатывает при отрисовке активного вагона
  try {
    if(preSelected && active_Wagon == wagon.parents(".wagon-wrapper").children(".wagon-name").text()) {
      for(i=0; i<preSelected.length;i++) {
        $("div[place="+preSelected[i]+"]",wagon).click();
      }
    }
  } catch(e) {
    // если массив с выделенными местами не был задан
  }
}


// Функция Создает табы для открытого типа вагонов
function wagonTabs(tabsKiosk) {
  tabsKiosk.tabs({
    select: function(event,ui) {
      // если имеются уже выбранные места, выводим предупреждение о том, что выделение будет снято и завершаем открытие нового таба.
      // Если пользователь соглашается, то вызываем открываем новый таб повторно
      if($(".wagon-section > div.checked").length) {
        var infoCaller = tabsKiosk.find(".wagon-tabs-title");
          closeInfoKiosks(function() {
            showInfoKiosk(infoCaller,"attention_alreadySelected");
        });
        $(".attention_accept").live("click",function(){ 
            $(".wagon-section > div.checked").each(function(){
              selectPlaces($(this));
            });
            tabsKiosk.tabs('select',ui.index); 
            closeInfoKiosks();
            $(".attention_accept").die("click");
            return false;
        });
        $(".attention_decline").live("click",function(){
            closeInfoKiosks();
            $(".attention_decline").die("click");
            return false;
        });
        return false;
      };
    },
    show: function(event,ui) {
      var wagonsQuantity = $(ui.tab).parent().siblings().length + 1;
      if(wagonsQuantity > 7) {
        $(ui.tab).parent().parent().parent().addClass("wagon-tabs-2StringTabs");
      }
      showPlaces($(ui.panel));
    }
  });
  if($('ul > li',tabsKiosk).length < 2) {
    $(".wagon-prev, .wagon-next, ul, .wagon-tabs-title",tabsKiosk).hide();
  }
  
  tabsKiosk.children(".wagon-next").click(function(){
   if($("ul.ui-tabs-nav > li",tabsKiosk).length > 1) {
      var activeTab = $("li.ui-tabs-selected",tabsKiosk);
      if(activeTab.next().length) {
        activeTab.next().children("a").click();
      } else {
        $("ul.ui-tabs-nav li:first-child a").click();
      }
    }
    return false;
  });
  tabsKiosk.children(".wagon-prev").click(function(){ 
    if($("ul.ui-tabs-nav > li",tabsKiosk).length > 1) {
      var activeTab = $("li.ui-tabs-selected",tabsKiosk);
      if(activeTab.prev().length) {
        activeTab.prev().children("a").click();
      } else {
         $("ul.ui-tabs-nav li:last-child a").click();
      }
    }
    return false;
  });
}


// Функция обработчик клика по месту
function selectPlaces(place) {
 
  // Если место отмечено как уже занятое, выходим
  if(place.attr("alreadybusy")=="true") return;
  
  // Если мест пытаются выбрать слишком много мест
  if($("div[place].checked",place.parents(".wagon-img")).length >= 4 && !place.hasClass("checked")) {
    var infoCaller = place;
    closeInfoKiosks(function () {
      showInfoKiosk(infoCaller,"attentionTooMany");
    });
    return;
  }
  
  if (place.parents("div[type='default']").length) return;
  
  // Отдельная обработка клика по месту 27 в 1 вагоне Сапсана
  // Проверка, не выделены ли еще другие места в вагоне
  if((place.attr("place")==27 || place.attr("place")==28 || place.attr("place")==29 || place.attr("place")==30) && (place.parents("#saps1_1.wagon-img").length || place.parents("#saps1_11.wagon-img").length)) {
    if($("div[place].checked:not([place=27]):not([place=28]):not([place=29]):not([place=30])",place.parents(".wagon-img")).length) {
      var infoCaller = place;
      closeInfoKiosks(function () {
        showInfoKiosk(infoCaller,"attentionSelect27");
      });
      return;
    }
  }
  
  // контейнеры с инфой о выбранных местах
  var infoDefault = place.parents(".wagon-scheme").siblings(".ticketData-container-default");
  var infoChoosed = place.parents(".wagon-scheme").siblings(".ticketData-container-selected");
    
  if(place.hasClass("checked")) {
    
    place.removeClass("checked");
     
    // удаляем место
    $(".ticketData-lineOptions",infoChoosed).has(".ticketData-place:contains('"+place.attr("place")+"')").remove();
    computeTotalPrice();
    if($(".ticketData-lineOptions",infoChoosed).length==1) {
      infoDefault.show();
      infoChoosed.hide();
    }
    
    // Раздизейбливаем все и дизейблим соседние места заново согласно правилам РЖД
    var placesSetAll = $("div[place]",place.parents(".wagon-img"));
    placesSetAll.removeClass("busy-sel");

    var placesSet = $("div[place].checked",place.parents(".wagon-img"));
    
    // Снятие выделения всех четырех мест 27-30 в 1 вагоне сапсана
    if((place.attr("place")==27 || place.attr("place")==28 || place.attr("place")==29 || place.attr("place")==30) && (place.parents("#saps1_1.wagon-img").length || place.parents("#saps1_11.wagon-img").length)) {
      for(i=27;i<=30;i++) {
        if(i!=place.attr("place") && $("div[place="+i+"].checked",place.parents(".wagon-img")).length) break;
      }
      $("div[place="+i+"].checked",place.parents(".wagon-img")).not("div[place="+place.attr("place")+"]").click();
    }

    
  } else {

    place.addClass("checked");
    
    // Добавляем место "в выбранные"
    var selectedPlace = {
      num: place.attr("place"),
      coupe_num: ( ( place.parents(".wagon-img").attr("type") == "plac" || place.parents(".wagon-img").attr("type") == "coupe" || place.parents(".wagon-img").attr("type") == "sv" || place.parents(".wagon-img").attr("type") == "sv_2f" || place.parents(".wagon-img").attr("type") == "coupe_2f" ) ? (parseInt(place.parent(".wagon-section").attr("num"))+1) + " купе, " : ""),
      price: place.attr("pricedisplay"),
      side: (place.attr("side")==1 ? "боковое" : ""),
      top: ( ( place.parents(".wagon-img").attr("type") == "plac" || place.parents(".wagon-img").attr("type") == "coupe" || place.parents(".wagon-img").attr("type") == "sv" || place.parents(".wagon-img").attr("type") == "sv_2f" || place.parents(".wagon-img").attr("type") == "coupe_2f" ) ? ( place.attr("top")==1 ? "верхнее" : "нижнее") : "" ),
      toilet: ""
    };
    $("#wagonPlacePrice").tmpl(selectedPlace).insertBefore(infoChoosed.children(".ticketData-total"));
    infoDefault.hide();
    infoChoosed.show();
    computeTotalPrice();

      // Выделение всех четырех мест 27-30 в 1 вагоне сапсана
    if ((place.attr("place") == 27 || place.attr("place") == 28 || place.attr("place") == 29 || place.attr("place") == 30) && (place.parents("#saps1_1.wagon-img").length || place.parents("#saps1_11.wagon-img").length)) {
        for(i=27;i<=30;i++) {
          if(i!=place.attr("place") && $("div[place="+i+"]:not(.checked)",place.parents(".wagon-img")).length) break;
        }
        $("div[place="+i+"]:not(.checked)",place.parents(".wagon-img")).not("div[place="+place.attr("place")+"]").click();
      }
    
  }
}

// Проверка выбранных мест согласно правилам РЖД
// ранее передавалась переменное "место" place. Сейчас передается переменная "вагон" wagon
function checkPlaces(wagon) {
    if (wagon.attr("type") == "plac" || wagon.attr("type") == "coupe" || wagon.attr("type") == "sv" || wagon.attr("type") == "coupe_2f" || wagon.attr("type") == "sv_2f" ) {
        placeLen = $("div[place].checked", wagon).length;
        if (placeLen > 1) {

            // Если выбрано зело много мест
            if (placeLen > 4) return 2;

            // Если выбраны места далее, чем на 4 свободных друг от друга
            if (placeLen > 1) {
                var j = 0;
                for (i = parseInt($("div[place].checked:first", wagon).attr("place")); i < parseInt($("div[place].checked:last", wagon).attr("place")); i++) {
                if ($("div[place=" + i + "]").attr("alreadybusy") != "true") j++;
                  if (j > 3) return 3;
                }
            }
        }
        // Если в вагоне предусмотрен выкуп пар мест
        if (wagon.attr("pair") && placeLen % 2 != 0) return 4;
    }
    return 0;
}


// Вычисляет общую сумму выбранных билетов
function computeTotalPrice() {
  var dataContainer = $(".ticketData-container-selected:visible");
  var totalPrice = 0;
  $(".ticketData-lineOptions:not(.ticketData-total)",dataContainer).each(function(){
    totalPrice += parseInt($(".ticketData-price",$(this)).text());
  })
  $(".ticketData-total .ticketData-totalPrice",dataContainer).text(totalPrice+" "+price_currency);
}

// Отмечает доступные и недоступные для покупки места по фильтру из функции applyFilter()
// при наличии уже выбранных мест потребуется согласие пользователя на снятие выделения
function checkFilterPlace(checkbox, places,agree) {
  var attentionList = "";
  // флаг необходимости снимать выделение с выбранных мест
  var flagQueryRequire = (places.hasClass("checked") && !places.hasClass("busy") && typeof agree == "undefined" ? 1 : 0);
  if(flagQueryRequire) {
    // список мест на снятие
    var i=0;
    places.each(function(){
      if($(this).hasClass("checked")) {
        attentionList += (i==0 ? "" : ", ")+$(this).attr("place");
        i++;
      }
    });
    $(".attentionFilterApply_wrapper span.places").text(attentionList);
    // запрос на снятие мест
    var infoCaller = checkbox;
    closeInfoKiosks(function() {
      showInfoKiosk(infoCaller,"attentionFilterApply");
    });
    $(".attentionFilterApply_accept").live("click",function(){ 
      checkFilterPlace(checkbox, places,1)
      $(".attention_accept").die("click");
      return false;
    });
    $(".attentionFilterApply_decline").live("click",function(){
      checkFilterPlace(checkbox, places,0)
      $(".attention_decline").die("click");
      return false;
    });
    return;
  }
  // согласие не получено, ничего не делаем, чекбокс не выделяем 
  if(agree==0) {
    checkbox.removeAttr("checked");
    return;
  }
  // согласие получено, выделение снимаем
  if(agree==1 || flagQueryRequire==1) {
    places.each(function() {
      if($(this).hasClass("checked"))
        selectPlaces($(this));
    });
  }
  // выделяем отфильтрованные места как недоступные
  places.addClass("busy");
}

// Функция обработчик клика по чекбоксу фильтра мест
function applyFilter(checkbox) {
  var checkboxContainer = checkbox.parents(".wagon-conditions");
  var wagonContainer = checkboxContainer.siblings(".wagon-tabs");
  var wagon = wagonContainer.children(".wagon-img:not(.ui-tabs-hide)");
  
  if (checkbox.attr("joint") || checkbox.attr("sit")) {
	if(checkbox.attr("checked")=="checked") {
	  $("input[type=checkbox]",checkboxContainer).each(function(){
        $(this).prop("disabled", true);
      });
    } else {
	}
  }
  
  if (checkbox.attr("joint") || checkbox.attr("sit")) return;
  
  if(checkbox.attr("checked")=="checked") {
    // выдялем места согласно фильтру
    checkFilterPlace(checkbox,getFilterCollection(checkbox));
  } else {
    // снимаем выделение, оставляя выделенными места для оставшихся фильтров
    var places_filterCollection = getFilterCollection(checkbox);
    if(places_filterCollection.length) {
      places_filterCollection.removeClass("busy");
    }
    $("input[type=checkbox]:checked",checkboxContainer).each(function(){
      checkFilterPlace(checkbox,getFilterCollection($(this)));
    });
  }
}

// Функция возвращает коллекцию мест для данного фильтра
function getFilterCollection(checkbox) {
  var wagonContainer = checkbox.parents(".wagon-conditions").siblings(".wagon-tabs");
  var wagon = wagonContainer.children(".wagon-img:not(.ui-tabs-hide)");
  switch(checkbox.attr('name')) {
    case('toilet') : {
      if(wagonContainer.hasClass("placTabs") || wagonContainer.hasClass("svTabs") || wagonContainer.hasClass("coupeTabs")) {
        return $(".wagon-section:last-child > div[place]",wagon);
      }
      break;
    }
    case('top') : {
      if(wagonContainer.hasClass("placTabs") || wagonContainer.hasClass("coupeTabs")) {
        return $(".wagon-section > div[top=0]",wagon);
      }
      break;
    }
    case('bottom') : {
      if(wagonContainer.hasClass("placTabs") || wagonContainer.hasClass("coupeTabs")) {
        return $(".wagon-section > div[top=1]",wagon);
      }
      break;
    }
    case('side') : {
      if(wagonContainer.hasClass("placTabs")) {
        return $(".wagon-section > div[side=1]",wagon);
      }
      break;
    }
    case('m') : {
      if(wagonContainer.hasClass("placTabs") || wagonContainer.hasClass("svTabs") || wagonContainer.hasClass("coupeTabs")) {
        return $(".wagon-section:not([gender='m']) > div[place]",wagon);
      }
      break;
    }
    case('f') : {
      if(wagonContainer.hasClass("placTabs") || wagonContainer.hasClass("svTabs") || wagonContainer.hasClass("coupeTabs")) {
        return $(".wagon-section:not([gender='f']) > div[place]",wagon);
      }
      break;
    }
    case('mix') : {
      if(wagonContainer.hasClass("placTabs") || wagonContainer.hasClass("svTabs") || wagonContainer.hasClass("coupeTabs")) {
        return $(".wagon-section:not([gender='mix']) > div[place]",wagon);
      }
      break;
    }
  }
  return false;
}

$(".ticketData-lineOptions button").live('click', function () {
    var wagon = $(this).parents(".wagon-wrapper").find(".wagon-img:not(.ui-tabs-hide)");
    var selectedPlacesError = checkPlaces(wagon);

    // Ошибка - выбранно зело много мест
    if (selectedPlacesError == 2) {
        var infoCaller = $(this).parents(".wagon-wrapper").find(".ticketData-lineOptions:not(.ticketData-total):last");
        closeInfoKiosks(function () {
            showInfoKiosk(infoCaller, "attentionTooMany");
      });
    }

    // Ошибка - выбранные места далеко друг от друга
    else if (selectedPlacesError == 3) {
        var infoCaller = $(this).parents(".wagon-wrapper").find(".ticketData-lineOptions:not(.ticketData-total):last");
        closeInfoKiosks(function () {
            showInfoKiosk(infoCaller, "attentionDiffPlaces");
        });

    }

    // Ошибка - выбранно недостатоно мест в вагоне с парным выкупом мест
    if (selectedPlacesError == 4) {
        var infoCaller = $(this).parents(".wagon-wrapper").find(".ticketData-lineOptions:not(.ticketData-total):last");
        closeInfoKiosks(function () {
            showInfoKiosk(infoCaller, "attentionPairsPlaces");
        });
    }

    else {
        var button = $(this);
        button.attr("disabled", "disabled");

        var places = [];
        $("div.checked").each(function () {
            places.push($(this).attr("place"));
        });

        var carNum = $("div.checked").first().parent().parent("div").attr("num");
		var placeRange = button.parents(".wagon-scheme").siblings("input[name='rangePlaceFrom']").val();
		placeRange += "_" + button.parents(".wagon-scheme").siblings("input[name='rangePlaceTo']").val();
		
		var adultCount = button.parents(".wagon-scheme").siblings("input[name='adult']").val();
		var childCount = button.parents(".wagon-scheme").siblings("input[name='child']").val();
		var babyCount = button.parents(".wagon-scheme").siblings("input[name='baby']").val();
	  		
        // показываем окно загрузки
        showLoadingWindow();

        $.ajax({
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            url: $(this).parent("form").attr("action"),
            data: JSON.stringify(
			{ 
				CarNum: carNum, 
				Places: places 
			}),
            success: function (data) {
                if (data.state) {
                    document.location.href = data.href;
                } else if (data.errors == "Session is null") {
                    document.location.href = "/";
                } else {
                    hideLoadingWindow();
                    showMessage(data.message, 300, 70);
                }
                button.removeAttr("disabled");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                hideLoadingWindow();
                button.removeAttr("disabled");
                showMessage(xhr.status + " : " + xhr.responseText, 300, 70);
            }
        });
    }
    return false;
});

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

$("span.wagon-legend-close, div.wagon-legend-open div.wagon-legend-title span").live("click",function(){
  var legendWrapper = $(this).siblings(".wagon-legend-wrapper");
  // если был клик по span
  if(!legendWrapper.hasClass("wagon-legend-wrapper")) legendWrapper = $(this).parent("div").siblings(".wagon-legend-wrapper");
  var allLegend = $(".wagon-legend");
  allLegend.removeClass("wagon-legend-open");
  legendWrapper.slideUp(function(){
    allLegend.addClass("wagon-legend-hidden");
  });
  var cookieDate = new Date( new Date().getTime() + 60*1000 );
  document.cookie = "legend=hide; path=/; expires: "+cookieDate.toUTCString();
})
$("div.wagon-legend-hidden div.wagon-legend-title span").live("click",function(){
  var legendWrapper = $(this).parent("div").siblings(".wagon-legend-wrapper");
  var allLegend = $(".wagon-legend");
  allLegend.addClass("wagon-legend-open");
  allLegend.removeClass("wagon-legend-hidden");
  legendWrapper.slideDown();
  document.cookie = "legend=show; path=/; expires: -1";
})