$(function(){
  var bodyHeight = $(window).height();
  var headerHeight = $("#headIn").outerHeight(); // alert($("#bottomWide").outerHeight() +"|"+ $("#bottomWide").height() +"|"+ $("#bottomWide").innerHeight());
  var footerHeight = $("#bottomWide").outerHeight();
  var contentHeight = $("#bodyWhite").outerHeight();
  var contentTextHeight = $(".content").outerHeight();
  var templateImages = 0;
  $("img.imgCenter").each(function(){
    templateImages += $(this).outerHeight();
  })
  var heightAll = headerHeight + footerHeight + contentHeight + templateImages;
  var difference = bodyHeight - heightAll;
  if(difference > 0)
    $(".content").height(parseInt(contentTextHeight+difference)+"px");
})
