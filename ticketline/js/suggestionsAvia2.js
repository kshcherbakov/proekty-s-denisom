var autoSuggestControl;

/**
* Provides suggestions for state names (USA).
* @class
* @scope public
*/
function CitySuggestions() {
}

/**
* Request suggestions for the given autosuggest control. 
* @scope protected
* @param oAutoSuggestControl The autosuggest control to provide suggestions for.
*/
CitySuggestions.prototype.requestSuggestions = function (oAutoSuggestControl /*:AutoSuggestControl*/) {
    autoSuggestControl = oAutoSuggestControl;

    var partiallyTypedValue = oAutoSuggestControl.textbox.value;
    var exlusion = oAutoSuggestControl.conjugateTextbox.value;
    oAutoSuggestControl.showLoadingPanel();
    TownAvia.GetCities(partiallyTypedValue, exlusion, this.GetCities_callback);
};
CitySuggestions.prototype.GetCities_callback = function(response /*:response object*/) {
    if (response.error != null) {
        alert(response.error);
        return;
    }
    var aSuggestions = string2JsonObj(response.value);
    //provide suggestions to the control
    autoSuggestControl.autosuggest(aSuggestions);
}