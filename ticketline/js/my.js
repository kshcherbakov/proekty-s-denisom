function clearClick(evt, str) {
    var obj = GetEventObject(evt);
    if (obj && obj.value && obj.value == str)
        obj.value = "";
}

function GetEventObject(evt) {
    evt = (evt) ? evt : ((window.event) ? event : null);
    if (evt) {
        var elem = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        return elem;
    }
    else alert("No evt or event");
}

function GetEventObjectTxt(evt) {
    evt = (evt) ? evt : ((window.event) ? event : null);
    if (evt) {
        var elem = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if (elem) {
            var txt = (elem.text) ? elem.text : ((elem.innerText) ? elem.innerText : null);
            if (txt)
                return txt;
            else alert("No text or innerText");
        }
        else alert("No target or srcElement");
    }
    else alert("No evt or event");
}

function string2JsonObj(string) {
    eval("var result = " + string);
    return result;
}

function printObject(obj) {
    var result = '';
    for (i in obj) {
        var str = " " + obj[i];
        if (str.indexOf("function") == -1)
            result += i + "=" + obj[i] + "\t";
    }

    alert(result);
}

function GetForm(cur_window) {
    var theform;
    if (cur_window.navigator.appName.toLowerCase().indexOf("netscape") > -1)
        theform = cur_window.document.forms["Form1"];
    else
        theform = cur_window.document.Form1;

    return theform;
}
function showObj(obj) {
    $("#" + obj).show();
}
function hideObj(obj) {
    $("#" + obj).hide();
}

String.prototype.format = function() {
    var txt = this;
    for (var i = 0; i < arguments.length; i++) {
        var exp = new RegExp('\\{' + (i) + '\\}', 'gm');
        txt = txt.replace(exp, arguments[i]);
    }
    return txt;
}

function InitTextBoxInputPrompt(textBoxID, inputPromptText) {
    var stringIsNullOrEmpty = function (str) {
        return !str ? true : str.replace(/(^\s+)|(\s+$)/g, "").length === 0;
    };

    $(document).ready(function () {
        var textBox = $("#" + textBoxID);
        var inputPromptID = 'inputPrompt_' + textBoxID;

        var onInputSpanClick = function () {
            $(this).hide();
            var input = $('.' + inputPromptID);
            input.mousedown();
            input.focus();
        };
        var onTextBoxFocus = function () {
            $("#" + inputPromptID).hide();
        };
        var onTextBoxBlur = function () {
            if (stringIsNullOrEmpty($(textBox).val()))
                $("#" + inputPromptID).show();
        };

        $(textBox).addClass(inputPromptID);

        var promptSpan = $('<span class="inputPrompt" />');
        if (!stringIsNullOrEmpty($(textBox).val())) $(promptSpan).hide();
        $(promptSpan).attr('id', inputPromptID);
        $(promptSpan).append(inputPromptText);
        $(textBox).before(promptSpan);

        $(textBox).blur(onTextBoxBlur);
        $(textBox).focus(onTextBoxFocus);
        $(promptSpan).click(onInputSpanClick);
    });
}