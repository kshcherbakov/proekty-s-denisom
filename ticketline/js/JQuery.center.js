﻿jQuery.fn.center = function() {
    this.makeAbsolute(true);
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
}

$.fn.makeAbsolute = function(rebase, top, left) {
    return this.each(function() {
        var el = $(this);
        
        el.css({ position: "absolute",
            marginLeft: 0, marginTop: 0,
            top: top, left: left
        });
        if (rebase)
            el.remove().appendTo("form");
    });
}