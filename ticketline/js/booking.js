$(document).ready(function(){

  $("div.ticket-body").hide();
  var showOrHide;
  $("div.ticket-headline").click(function(){
    $(this).siblings("div.ticket-body").toggle(showOrHide);
  });

  $("div.ticket-variant input").click(function(){
    var parentChoice = $(this).parents("div.ticket-choice");
    var parentContainer = parentChoice.parents("div.ticket-variant").parents("div.ticket-column");

    parentContainer.children("div.ticket-variant").each(function(){
      $(this).children("div.ticket-choice").removeClass("ticket-choice-selected");
    });
    parentChoice.addClass("ticket-choice-selected");
  });
  $("div.ticket-item input:checked").each(function(){
    $(this).click();
  });

});